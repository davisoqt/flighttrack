package leoperez.com.flighttrack.data_layer.repository;

import android.util.Log;

import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Optional;
import java.util.concurrent.BrokenBarrierException;
import java.util.stream.Stream;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.domain_layer.converter.ConverterModel;
import leoperez.com.flighttrack.domain_layer.converter.CrewCostItem;
import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.domain_layer.converter.TripBasicData;
import leoperez.com.flighttrack.domain_layer.db.entity.DaoSession;
import leoperez.com.flighttrack.domain_layer.db.entity.FTTripDetail;
import leoperez.com.flighttrack.domain_layer.response.save_accost.Request;
import leoperez.com.flighttrack.domain_layer.response.save_accost.ReturnDatum;
import leoperez.com.flighttrack.domain_layer.response.save_accost.SaveAcCostResponse;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.domain_layer.response.trip.Result;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

public class TripsDetailRepository {

    @Inject
    DaoSession session;

    @Inject
    public TripsDetailRepository() {
    }

    public Completable saveTripDetail(Result trip){
        return  Completable.create(e -> {
            session.getFTTripDetailDao().insertOrReplace(new FTTripDetail(new Gson().toJson(trip),0));
            e.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    public io.reactivex.Observable<ArrayList<LegItem>> getLegsForList(){
        return io.reactivex.Observable.create(e -> {
            Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
            ArrayList<LegItem> data= new ArrayList<>();
            for (Leg leg :
                    result.getLegs()) {
                data.add(ConverterModel.converToLegItem(leg));
            }
            e.onNext(data);
            e.onComplete();
        });
    }

    public io.reactivex.Observable getLegs(){
        return io.reactivex.Observable.create(e -> {
            Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
            if(Constants.DEBUG) {
                String json=new Gson().toJson(result);
                Log.d("Repository trip", "getLegs: " + new Gson().toJson(result));
            }
            e.onNext(result.getLegs());
            result=null;
            e.onComplete();
        });
    }

    public List<Leg> getLegsSync(){
            Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
            if(Constants.DEBUG) {
                String json=new Gson().toJson(result);
                Log.d("Repository trip", "getLegs: " + new Gson().toJson(result));
            }
            return result.getLegs();
    }

    public io.reactivex.Observable<ArrayList<CrewCostItem>> getCrewCostItems(){
      return   io.reactivex.Observable.create((ObservableOnSubscribe<ArrayList<CrewCostItem>>) e -> {
            Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
            ArrayList<CrewCostItem> data= new ArrayList<>();
            com.annimon.stream.Stream.of(result.getCrewCostsList()).forEach(crewCostsList -> {
                data.add(ConverterModel.convertToCrewCostItem(crewCostsList));
            });
            e.onNext(data);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
    }

    public ArrayList<String> getCrewCostIds(){
        Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
        ArrayList<String> data= new ArrayList<>();
        com.annimon.stream.Stream.of(result.getCrewCostsList()).forEach(crewCostsList -> {
            data.add(crewCostsList.getCrewID());
        });
        return data;
    }

    public ArrayList<String> getICAO(){
        Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
        return result.getTripICAOList();
    }
    public CrewCostsList getCrewCost(String id){
        Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
        com.annimon.stream.Optional<CrewCostsList> oprional=com.annimon.stream.Stream.of(result.getCrewCostsList()).filter(value -> value.getCrewID().equals(id)).findFirst();
        if(oprional.isPresent())
            return oprional.get();
        return null;
    }

    public TripBasicData getBasicData(){
        Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
        return new TripBasicData(result.getTripID(),result.getStartDate(),result.getLastUpdated(),result.getAircraftID());
    }

    public boolean isOld() {
        Result result = new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(), Result.class);
        DateTime time = DateTimeFormat.forPattern("MM/dd/yy").parseDateTime(result.getStartDate());
        DateTime today= new DateTime();
        if (today.getMillis() > time.getMillis())
            return true;
        return false;
    }

    public List<Leg> updateLeg(ReturnDatum response){
        Request request= response.getResult().getRequest();
        List<Leg> legs= getLegsSync();
        int position=0;
        for (Leg leg :
                legs) {
            if(leg.getLegNumber().equals(request.getLegNumber()))
                break;
            position++;
        }
        if(request.getADFlag().equals("D")){
            legs.get(position).getACCostDepart().setFBO(request.getFBO());
            legs.get(position).getACCostDepart().setQuantity(request.getQuantity());
            legs.get(position).getACCostDepart().setDate(request.getCostDate());
            legs.get(position).getACCostDepart().setUnits(request.getUnits());
            legs.get(position).getACCostDepart().setAmount(request.getAmount());
            legs.get(position).getACCostDepart().setCCard(request.getCCard());
            legs.get(position).getACCostDepart().setRetailCost(request.getRetailCost());
            legs.get(position).getACCostDepart().setNotes(request.getNotes());
            legs.get(position).getACCostDepart().setACCostFieldList(request.getACCostFieldList());
        }else {
            legs.get(position).getACCostArrive().setFBO(request.getFBO());
            legs.get(position).getACCostArrive().setQuantity(request.getQuantity());
            legs.get(position).getACCostArrive().setDate(request.getCostDate());
            legs.get(position).getACCostArrive().setUnits(request.getUnits());
            legs.get(position).getACCostArrive().setAmount(request.getAmount());
            legs.get(position).getACCostArrive().setCCard(request.getCCard());
            legs.get(position).getACCostArrive().setRetailCost(request.getRetailCost());
            legs.get(position).getACCostArrive().setNotes(request.getNotes());
            legs.get(position).getACCostArrive().setACCostFieldList(request.getACCostFieldList());
        }
        Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
        result.setLegs(legs);
        session.getFTTripDetailDao().insertOrReplace(new FTTripDetail(new Gson().toJson(result),0));
        return legs;
    }

    public List<Leg> updateLeg(leoperez.com.flighttrack.domain_layer.payload.save_crew.Request request)
    {
        List<Leg> legs= getLegsSync();
        int position=0;
        for (Leg leg :
                legs) {
            if(leg.getLegNumber().equals(request.getLegNumber()))
                break;
            position++;
        }
        legs.get(position).getCrew().setCrewFieldList((ArrayList<CrewFieldList>) request.getCrewFieldList());
        legs.get(position).getCrew().setACM1Id(request.getCrew().getACM1Id());
        legs.get(position).getCrew().setACM2Id(request.getCrew().getACM2Id());
        legs.get(position).getCrew().setACM3Id(request.getCrew().getACM3Id());
        legs.get(position).getCrew().setACM4Id(request.getCrew().getACM4Id());
        legs.get(position).getCrew().setPICFlying(request.getCrew().getPICFlying());
        legs.get(position).getCrew().setSICFlying(request.getCrew().getSICFlying());
        legs.get(position).getCrew().setPICId(request.getCrew().getPICId());
        legs.get(position).getCrew().setSICId(request.getCrew().getSICId());
        Result result= new Gson().fromJson(session.getFTTripDetailDao().loadAll().get(0).getJson(),Result.class);
        result.setLegs(legs);
        session.getFTTripDetailDao().insertOrReplace(new FTTripDetail(new Gson().toJson(result),0));
        return legs;
    }
}
