package leoperez.com.flighttrack.data_layer.repository;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.domain_layer.db.entity.DaoSession;
import leoperez.com.flighttrack.domain_layer.db.entity.FTTripListTB;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.Trips;

/**
 * Created by Leo Perez Ortíz on 31/10/17.
 */

public class TripListRepository {
    private static final String TAG = "TripListRepository";
    @Inject
    DaoSession session;

    @Inject
    public TripListRepository() {
    }

    public void insertTripList(Trips trips) {
        Completable.create(e -> {
            session.getFTTripListTBDao().insertOrReplace(new FTTripListTB(new Gson().toJson(trips), 0));
        }).subscribeOn(Schedulers.newThread())
                .subscribe(() -> {
                    Log.d(TAG, "insertTripList: OK");
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.d(TAG, "insertTripList: FAIL");
                });
    }
    public void insertTripListSync(Trips trips) {
            session.getFTTripListTBDao().insertOrReplace(new FTTripListTB(new Gson().toJson(trips), 0));
    }

    public Observable<Object> getTripList(){
        return Observable.create(e -> {
            List<FTTripListTB> x=session.getFTTripListTBDao().loadAll();
            FTTripListTB row= session.getFTTripListTBDao().loadAll().get(0);
            if(row!=null){
                Trips trips= new Gson().fromJson(row.getJson(),Trips.class);
                e.onNext(trips.getTripDetails());
            }else {
                e.onNext("");
            }
            e.onComplete();
        }).subscribeOn(Schedulers.newThread());

    }
}
