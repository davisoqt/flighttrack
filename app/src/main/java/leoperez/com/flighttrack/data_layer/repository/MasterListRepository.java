package leoperez.com.flighttrack.data_layer.repository;

import com.google.gson.Gson;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.domain_layer.db.entity.DaoSession;
import leoperez.com.flighttrack.domain_layer.db.entity.FTMasterListTB;
import leoperez.com.flighttrack.domain_layer.response.home.masterlist.Result;
import leoperez.com.flighttrack.domain_layer.response.home.masterlist.UpdateMasterList;

/**
 * Created by Leo Perez Ortíz on 30/10/17.
 */

public class MasterListRepository {

    @Inject
    DaoSession session;

    @Inject
    public MasterListRepository() {
    }

    public void saveFTMasterlist(UpdateMasterList masterList){
        Completable.create(e -> {
            session.getFTMasterListTBDao().insertOrReplace(new FTMasterListTB(new Gson().toJson(masterList),0L));
        }).subscribeOn(Schedulers.newThread())
                .subscribe(() -> {},throwable -> throwable.printStackTrace());
    }
    public void saveFTMasterlistSync(UpdateMasterList masterList){
            session.getFTMasterListTBDao().insertOrReplace(new FTMasterListTB(new Gson().toJson(masterList),0L));
    }

    public ArrayList<String> getCrewIds(){
        String json=session.getFTMasterListTBDao().loadAll().get(0).getJson();
        UpdateMasterList result= new Gson().fromJson(json,UpdateMasterList.class);
        return result.getCrewIDList().getCrewID();
    }

    public ArrayList<String> getCreditCards(){
        String json=session.getFTMasterListTBDao().loadAll().get(0).getJson();
        UpdateMasterList result= new Gson().fromJson(json,UpdateMasterList.class);
        ArrayList<String> cards= result.getCCList().getCCName();
        cards.add(0,"SELECT");
        return cards;
    }

    public ArrayList<String> getAMC(){
        String json=session.getFTMasterListTBDao().loadAll().get(0).getJson();
        UpdateMasterList result= new Gson().fromJson(json,UpdateMasterList.class);
        return result.getCrewIDList().getCrewID();
    }
}
