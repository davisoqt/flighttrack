package leoperez.com.flighttrack.app;

import android.content.Context;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Leo Perez Ortíz on 29/10/17.
 */

public class Constants {
    public static final boolean DEBUG = false;

    //Preff
    public static final String SESSION_KEY="key";
    public static final String SAVE_CREDENTIAL="save";
    public static final String CUSTOM_CODE="code";
    public static final String NAME="name";


    public static void launchActivityFinish(Context context,Class clas){
        context.startActivity(new Intent(context,clas));
        ((AppCompatActivity)context).finish();
    }
}
