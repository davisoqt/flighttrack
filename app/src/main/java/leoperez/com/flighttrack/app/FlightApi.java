package leoperez.com.flighttrack.app;

import leoperez.com.flighttrack.domain_layer.db.entity.FTTripDetailDao;
import leoperez.com.flighttrack.domain_layer.payload.PayLoadLogin;
import leoperez.com.flighttrack.domain_layer.payload.home.masterList.FtMasterListPayLoad;
import leoperez.com.flighttrack.domain_layer.payload.home.trip.PayLoadTripList;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.AcCostSave;
import leoperez.com.flighttrack.domain_layer.payload.save_crew.SaveCrew;
import leoperez.com.flighttrack.domain_layer.payload.trip.PayLoadTripDetail;
import leoperez.com.flighttrack.domain_layer.response.home.masterlist.FtMasterListResponse;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.FtTripListResponse;
import leoperez.com.flighttrack.domain_layer.response.login.LoginResponse;
import leoperez.com.flighttrack.domain_layer.response.save_accost.SaveAcCostResponse;
import leoperez.com.flighttrack.domain_layer.response.save_crew.SaveCrewResponse;
import leoperez.com.flighttrack.domain_layer.response.trip.TripDetailResponse;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Leo Perez Ortíz on 29/10/17.
 */

public interface FlightApi {

    @POST("suggestus.php")
    io.reactivex.Observable<LoginResponse> login(@Body PayLoadLogin loadLogin);

    @POST("suggestus.php")
    io.reactivex.Observable<FtMasterListResponse> getMasterList(@Body FtMasterListPayLoad loadLogin);

    @POST("suggestus.php")
    io.reactivex.Observable<FtTripListResponse> getTripList(@Body PayLoadTripList loadLogin);
//
//    @GET("https://flighttrak-jsons-test.s3.amazonaws.com/getFTTripList.json")
//    io.reactivex.Observable<FtTripListResponse> getTripList();

    @POST("suggestus.php")
    io.reactivex.Observable<TripDetailResponse> getTripDetail(@Body PayLoadTripDetail loadLogin);

    @POST("suggestus.php")
    io.reactivex.Observable<SaveAcCostResponse> saveAcCost(@Body AcCostSave acCostSave);

    @POST("suggestus.php")
    io.reactivex.Observable<SaveCrewResponse> saveCrew(@Body SaveCrew saveCrew);
}
