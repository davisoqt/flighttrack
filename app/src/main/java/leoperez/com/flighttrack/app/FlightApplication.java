package leoperez.com.flighttrack.app;

import android.app.Application;

import com.blankj.utilcode.util.Utils;

import java.util.ArrayList;

import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.dagger.components.AppComponent;
import leoperez.com.flighttrack.dagger.components.DaggerAppComponent;
import leoperez.com.flighttrack.dagger.modules.ApiModule;
import leoperez.com.flighttrack.dagger.modules.AppModule;
import leoperez.com.flighttrack.dagger.modules.DBModule;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;

/**
 * Created by Leo Perez Ortíz on 27/10/17.
 */

public class FlightApplication extends Application {

    public static String trip_ID;
    private AppComponent appComponent;
    public static ArrayList<String> credit_cards;
    public static ArrayList<LegSpinnerItem> legs;
    public static boolean isTablet;

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.init(this);
        appComponent = DaggerAppComponent.builder()
//                 list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .dBModule(new DBModule(this))
                .apiModule(new ApiModule())
                .build();
        isTablet= getResources().getBoolean(R.bool.is_tablet);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}