package leoperez.com.flighttrack.dagger.modules;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import leoperez.com.flighttrack.domain_layer.db.entity.DaoMaster;
import leoperez.com.flighttrack.domain_layer.db.entity.DaoSession;

/**
 * Created by Leo Perez Ortíz on 27/10/17.
 */

@Module
public class DBModule {

    protected String dBName="flight";
    Context c;

    public DBModule(Context c) {
        this.c = c;
    }

    @Provides
    public DaoSession provideDaoSession(){
        DaoMaster.DevOpenHelper devOpenHelper= new DaoMaster.DevOpenHelper(c,dBName);
        SQLiteDatabase sqLiteDatabase= devOpenHelper.getWritableDatabase();
        DaoMaster daoMaster= new DaoMaster(sqLiteDatabase);
        return daoMaster.newSession();
    }
}
