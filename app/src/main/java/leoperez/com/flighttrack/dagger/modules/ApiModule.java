package leoperez.com.flighttrack.dagger.modules;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import leoperez.com.flighttrack.app.FlightApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Leo Perez Ortíz on 27/10/17.
 */

@Module
public class ApiModule {

    @Singleton
    @Provides
    Retrofit provideRetrofit(){
        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl("http://flighttrak.online:81/suggestus/")
                        .addConverterFactory(GsonConverterFactory.create());
        OkHttpClient httpClient =
                new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60,TimeUnit.SECONDS).build();
        builder.client(httpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        return builder.build();
    }

    @Provides
    FlightApi provideFlightApi(Retrofit retrofit){
        return retrofit.create(FlightApi.class);
    }

}
