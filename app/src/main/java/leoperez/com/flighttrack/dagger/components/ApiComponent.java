package leoperez.com.flighttrack.dagger.components;

import dagger.Component;
import leoperez.com.flighttrack.dagger.modules.ApiModule;

/**
 * Created by Leo Perez Ortíz on 27/10/17.
 */
@Component(modules = ApiModule.class)
public interface ApiComponent {
}
