package leoperez.com.flighttrack.dagger.components;

import android.support.v4.app.Fragment;

import javax.inject.Singleton;

import dagger.Component;
import leoperez.com.flighttrack.presentation_layer.presenter.ACostPresenter;
import leoperez.com.flighttrack.presentation_layer.view.activity.CheckOutActivity;
import leoperez.com.flighttrack.presentation_layer.view.activity.HomeActivity;
import leoperez.com.flighttrack.dagger.modules.ApiModule;
import leoperez.com.flighttrack.dagger.modules.AppModule;
import leoperez.com.flighttrack.dagger.modules.DBModule;
import leoperez.com.flighttrack.presentation_layer.view.activity.LoginActivity;
import leoperez.com.flighttrack.presentation_layer.view.activity.TripDetailActivity;
import leoperez.com.flighttrack.presentation_layer.view.fragment.ACostFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewCostDetailFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewCostFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.LegFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.PaxFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.TripDetailFragment;

/**
 * Created by Leo Perez Ortíz on 27/10/17.
 */

@Component(modules = {DBModule.class,AppModule.class, ApiModule.class})
@Singleton
public interface AppComponent {
    void inject(HomeActivity object);
    void inject(LoginActivity object);
    void inject(TripDetailFragment object);
    void inject(TripDetailActivity object);
    void inject(CheckOutActivity object);
    void inject(CrewCostFragment object);
    void inject(CrewCostDetailFragment object);
    void inject(CrewFragment object);
    void inject(PaxFragment object);
    void inject(ACostFragment object);
    void inject(LegFragment object);
}
