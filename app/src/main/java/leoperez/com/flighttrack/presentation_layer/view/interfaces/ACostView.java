package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public interface ACostView extends BaseView {
    void showSucces();

    void setLegs(ArrayList<Leg> o);

    void setLegItemData(ArrayList<LegSpinnerItem> legSpinnerItems);

    void isOld(boolean old);

    void showCodeError(String statusMessage);
}
