package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Stream;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;

import net.grandcentrix.thirtyinch.TiFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.CrewCostItem;
import leoperez.com.flighttrack.domain_layer.converter.RowPosition;
import leoperez.com.flighttrack.presentation_layer.presenter.CrewCostPresenter;
import leoperez.com.flighttrack.presentation_layer.view.activity.CheckOutActivity;
import leoperez.com.flighttrack.presentation_layer.view.adapter.CrewCostAdapter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.CrewCostView;


public class CrewCostFragment extends TiFragment<CrewCostPresenter, CrewCostView> implements CrewCostView,View.OnClickListener {

    @Inject
    CrewCostPresenter presenter;
    MaterialDialog materialDialog;
//    @BindView(R.id.leg_container)
//    LinearLayout legContainer;
//    @BindView(R.id.plane1_container)
//    LinearLayout plane1Container;
//    @BindView(R.id.plane2_container)
//    LinearLayout plane2Container;
//    @BindView(R.id.check_container)
//    LinearLayout checkContainer;
    @BindView(R.id.rv)
    RecyclerView recyclerView;
    Unbinder unbinder;
    private LayoutInflater inflater;
    private CheckOutActivity context;

    public CrewCostFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CrewCostFragment newInstance() {
        CrewCostFragment fragment = new CrewCostFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((FlightApplication)getActivity().getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_crew_cost, container, false);
        unbinder = ButterKnife.bind(this, view);
        int screwnWith= ScreenUtils.getScreenWidth();
        if(FlightApplication.isTablet) {
            view.findViewById(R.id.header1).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.header2).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.15), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.header3).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.15), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.header4).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.15), ViewGroup.LayoutParams.MATCH_PARENT));
        }else {
            view.findViewById(R.id.header1).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.40), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.header2).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.header3).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.header4).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
        }
        initLoading();
        return view;
    }

    @NonNull
    @Override
    public CrewCostPresenter providePresenter() {
        return presenter;
    }

    @Override
    public void shoLoadng() {
        materialDialog.show();
    }

    @Override
    public void hideLoading() {
        materialDialog.dismiss();
    }

    @Override
    public void initLoading() {
        materialDialog = new MaterialDialog.Builder(getContext())
                .content("Loading")
                .progress(true, 30)
                .build();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=(CheckOutActivity)context;
    }

    @Override
    public void showError(String error_trip_detail) {

    }

    @Override
    public void initList(ArrayList<CrewCostItem> items) {
        recyclerView.setAdapter(new CrewCostAdapter(items,context));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {
        context.showCrewCostDetail((String) view.getTag());
    }
}
