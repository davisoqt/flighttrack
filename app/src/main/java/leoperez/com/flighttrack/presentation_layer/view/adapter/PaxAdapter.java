package leoperez.com.flighttrack.presentation_layer.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ScreenUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.response.trip.PaxListTrip;

/**
 * Created by Leo Perez Ortíz on 10/11/17.
 */

public class PaxAdapter extends RecyclerView.Adapter<PaxAdapter.ViewHolder> {

    ArrayList<PaxListTrip> items;

    public PaxAdapter(ArrayList<PaxListTrip> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pax_name, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int screwnWith= ScreenUtils.getScreenWidth();
        view.findViewById(R.id.column1).setLayoutParams(new LinearLayout.LayoutParams((int)(screwnWith*0.5), ViewGroup.LayoutParams.MATCH_PARENT));
        view.findViewById(R.id.column2).setLayoutParams(new LinearLayout.LayoutParams((int)(screwnWith*0.25), ViewGroup.LayoutParams.MATCH_PARENT));
        view.findViewById(R.id.column3).setLayoutParams(new LinearLayout.LayoutParams((int)(screwnWith*0.25), ViewGroup.LayoutParams.MATCH_PARENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView6.setText(items.get(position).getPaxId());
        holder.textView5.setText(items.get(position).getPaxPhone1());
        holder.textView7.setText(items.get(position).getPaxPhone2());
        if(TextUtils.isEmpty(items.get(position).getPaxPhone1()))
            holder.textView5.setVisibility(View.INVISIBLE);
        else
            holder.textView5.setVisibility(View.VISIBLE);
        if(TextUtils.isEmpty(items.get(position).getPaxPhone2()))
            holder.textView7.setVisibility(View.INVISIBLE);
        else
            holder.textView7.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView6)
        TextView textView6;
        @BindView(R.id.textView5)
        TextView textView5;
        @BindView(R.id.textView7)
        TextView textView7;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
