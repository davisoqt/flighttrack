package leoperez.com.flighttrack.presentation_layer.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import leoperez.com.flighttrack.R;

/**
 * Created by Leo Perez Ortíz on 1/8/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
//        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void iniDagger(){};

}
