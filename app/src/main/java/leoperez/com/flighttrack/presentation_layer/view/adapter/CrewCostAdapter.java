package leoperez.com.flighttrack.presentation_layer.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ScreenUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.CrewCostItem;
import leoperez.com.flighttrack.domain_layer.response.trip.PaxListTrip;
import leoperez.com.flighttrack.presentation_layer.view.activity.CheckOutActivity;

/**
 * Created by Leo Perez Ortíz on 10/11/17.
 */

public class CrewCostAdapter extends RecyclerView.Adapter<CrewCostAdapter.ViewHolder> {

    ArrayList<CrewCostItem> items;
    private CheckOutActivity context;

    public CrewCostAdapter(ArrayList<CrewCostItem> items, CheckOutActivity context) {
        this.items = items;
        this.context = context;
    }

    public CrewCostAdapter(ArrayList<CrewCostItem> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crewcost_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int screwnWith= ScreenUtils.getScreenWidth();
        if(FlightApplication.isTablet) {
            view.findViewById(R.id.column1).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.column2).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.15), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.column3).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.15), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.column4).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.15), ViewGroup.LayoutParams.MATCH_PARENT));
        }else {
            view.findViewById(R.id.column1).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.40), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.column2).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.column3).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
            view.findViewById(R.id.column4).setLayoutParams(new LinearLayout.LayoutParams((int) (screwnWith * 0.20), ViewGroup.LayoutParams.MATCH_PARENT));
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView6.setText(items.get(position).getCrew_id());
        holder.textView5.setText(items.get(position).getDate());
        holder.textView7.setText(items.get(position).getIcao());
        holder.textView8.setText(items.get(position).getCredit_card());
        holder.itemView.setTag(items.get(position).getCrew_id());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView6)
        TextView textView6;
        @BindView(R.id.textView5)
        TextView textView5;
        @BindView(R.id.textView7)
        TextView textView7;
        @BindView(R.id.textView8)
        TextView textView8;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> context.showCrewCostDetail((String) view.getTag()));
        }
    }
}

