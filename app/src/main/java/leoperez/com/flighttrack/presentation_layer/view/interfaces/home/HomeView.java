package leoperez.com.flighttrack.presentation_layer.view.interfaces.home;

import net.grandcentrix.thirtyinch.TiView;
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread;

import java.util.ArrayList;
import java.util.List;

import leoperez.com.flighttrack.domain_layer.response.home.triplist.TripDetail;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.Trips;

/**
 * Created by Leo Perez Ortíz on 31/10/17.
 */

public interface HomeView extends TiView {

    void showLOading(String text);

    void loadList(ArrayList<TripDetail> trips);

    void hideDialog();

    void showErrorSection();

    void emptyContent();
}
