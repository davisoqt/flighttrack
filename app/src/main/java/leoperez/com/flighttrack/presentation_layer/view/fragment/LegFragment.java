package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import net.grandcentrix.thirtyinch.TiFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindBool;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.converter.RowPosition;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.domain_layer.response.trip.LegFieldListTrip;
import leoperez.com.flighttrack.presentation_layer.presenter.LegPresenter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerAdapter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.LegView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LegFragment#newInstance} factory method to
 * create an instance of this Ti.
 */
public class LegFragment extends TiFragment<LegPresenter, LegView> implements LegView {

    private static final String TAG = LegFragment.class.getCanonicalName();
    @Inject
    LegPresenter presenter;
    @BindView(R.id.textView18)
    TextView textView18;
    @BindView(R.id.textView17)
    TextView textView17;
    @BindView(R.id.textView16)
    TextView textView16;
    @BindView(R.id.spinner4)
    Spinner spinner4;
    @BindView(R.id.container)
    LinearLayout container;
    @BindBool(R.bool.is_tablet)
    boolean isTablet;
    Unbinder unbinder;
    private ArrayList<Leg> legs;
    private LayoutInflater inflater;
    private View selected_view;
    private boolean isOld;
    private LinearLayout column2;
    private LinearLayout column1;

    public LegFragment() {
        // Required empty public constructor
    }

    public static LegFragment newInstance() {
        LegFragment fragment = new LegFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((FlightApplication)getActivity().getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        this.inflater=inflater;
        View view = inflater.inflate(R.layout.fragment_leg, container, false);
        unbinder = ButterKnife.bind(this, view);
        if(isTablet){
            column1= view.findViewById(R.id.c1);
            column2= view.findViewById(R.id.c2);
            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth()*0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            RelativeLayout.LayoutParams lpp=new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth()*0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lpp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            view.findViewById(R.id.c2).setLayoutParams(lp);
            view.findViewById(R.id.c1).setLayoutParams(lpp);
        }
        return view;
    }

    @Override
    public void shoLoadng() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void initLoading() {

    }

    @Override
    public void showError(String error_trip_detail) {

    }

    @NonNull
    @Override
    public LegPresenter providePresenter() {
        return presenter;
    }

    @Override
    public void setLegs(ArrayList<Leg> legs) {
        this.legs = legs;
    }

    @Override
    public void setLegItemData(ArrayList<LegSpinnerItem> items) {
        spinner4.setAdapter(new SpinnerAdapter(items, getContext()));
        new Handler().postDelayed(() -> spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadDinamic();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        }),500);
    }

    @Override
    public void loadDinamic() {
        if(!isTablet) {
            container.removeAllViews();
            Flowable.create(e -> {
                        for (LegFieldListTrip field : legs.get(spinner4.getSelectedItemPosition()).getLegFieldListTrip()
                                ) {
                            View item_view = inflater.inflate(R.layout.edittext_item, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.editText);
                            edit.setHint("Enter " + field.getFieldLabel());
                            if (!TextUtils.isEmpty(field.getFieldValue()))
                                edit.setText(field.getFieldValue());
                            //InputType
                            Log.d(TAG, "init: fielType " + field.getFieldType());
                            if (field.getFieldType().equals("A"))
                                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                            if (field.getFieldType().equals("I"))
                                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                            if (field.getFieldType().equals("T")) {
                                edit.setFocusable(false);
                                edit.setFocusableInTouchMode(false);
                                edit.setOnClickListener(view -> {
                                    selected_view = view;
                                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                                            .setStartTime(10, 10)
                                            .setOnTimeSetListener((dialog, hourOfDay, minute) -> {
                                                ((EditText) selected_view).setText(hourOfDay + ":" + minute);
                                            })
                                            .setDoneText("Ok")
                                            .setCancelText("Cancel");
                                    rtpd.show(getChildFragmentManager(), "d");
                                });
                            }
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    if (field.getFieldType().equals("T"))
                                        ((EditText) view.getTag()).performClick();
                                    else
                                        KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            } else {
                                edit.setEnabled(false);
                            }
                            e.onNext(item_view);
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        container.addView((View) item_view);
                    }, throwable -> throwable.printStackTrace());
        }
        else {
            final int[] position = {0};
            column1.removeAllViews();
            column2.removeAllViews();
            Flowable.create(e -> {
                        for (LegFieldListTrip field : legs.get(spinner4.getSelectedItemPosition()).getLegFieldListTrip()
                                ) {
                            View item_view = inflater.inflate(R.layout.edittext_item, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.editText);
                            edit.setHint("ENTER DATA " );
                            if (!TextUtils.isEmpty(field.getFieldValue()))
                                edit.setText(field.getFieldValue());
                            //InputType
                            Log.d(TAG, "init: fielType " + field.getFieldType());
                            if (field.getFieldType().equals("A"))
                                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                            if (field.getFieldType().equals("I"))
                                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                            if (field.getFieldType().equals("T")) {
                                edit.setFocusable(false);
                                edit.setFocusableInTouchMode(false);
                                edit.setOnClickListener(view -> {
                                    selected_view = view;
                                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                                            .setStartTime(10, 10)
                                            .setOnTimeSetListener((dialog, hourOfDay, minute) -> {
                                                ((EditText) selected_view).setText(hourOfDay + ":" + minute);
                                            })
                                            .setDoneText("Ok")
                                            .setCancelText("Cancel");
                                    rtpd.show(getChildFragmentManager(), "d");
                                });
                            }
                            //click label
                            if(!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    if (field.getFieldType().equals("T"))
                                        ((EditText) view.getTag()).performClick();
                                    else
                                        KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            }else {
                                edit.setEnabled(false);
                            }
                            e.onNext(new RowPosition(position[0]++,item_view));
                            if(position[0]==2)
                                position[0]=0;
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        switch (((RowPosition)item_view).getPosition()){
                            case 0:
                                column1.addView(((RowPosition)item_view).getContent());
                                break;
                            case 1:
                                column2.addView(((RowPosition)item_view).getContent());
                                break;
                        }
                    }, throwable -> throwable.printStackTrace());
        }
    }

    @Override
    public void isOld(boolean old) {
        this.isOld=old;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
