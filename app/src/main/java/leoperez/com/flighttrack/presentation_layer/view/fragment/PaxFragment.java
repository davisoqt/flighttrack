package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.blankj.utilcode.util.ScreenUtils;

import net.grandcentrix.thirtyinch.TiFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.domain_layer.response.trip.PaxListTrip;
import leoperez.com.flighttrack.presentation_layer.presenter.PaxPresenter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.PaxAdapter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerAdapter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.PaxView;

public class PaxFragment extends TiFragment<PaxPresenter, PaxView> implements PaxView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Unbinder unbinder;
    @BindView(R.id.spinner4)
    Spinner spinner4;
    @BindView(R.id.rv)
    RecyclerView rv;
    private ArrayList<Leg> legs;
    private ArrayList<PaxListTrip> paxs;
    private LayoutInflater inflater;
    @Inject
    PaxPresenter presenter;

    public PaxFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PaxFragment newInstance() {
        PaxFragment fragment = new PaxFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((FlightApplication) getActivity().getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_pax, container, false);
        this.inflater = inflater;
        unbinder = ButterKnife.bind(this, view);
        int screwnWith= ScreenUtils.getScreenWidth();
        view.findViewById(R.id.column1).setLayoutParams(new LinearLayout.LayoutParams((int)(screwnWith*0.5), ViewGroup.LayoutParams.MATCH_PARENT));
        view.findViewById(R.id.column2).setLayoutParams(new LinearLayout.LayoutParams((int)(screwnWith*0.25), ViewGroup.LayoutParams.MATCH_PARENT));
        view.findViewById(R.id.column3).setLayoutParams(new LinearLayout.LayoutParams((int)(screwnWith*0.25), ViewGroup.LayoutParams.MATCH_PARENT));
        return view;
    }

    @Override
    public void init() {
        paxs = legs.get(spinner4.getSelectedItemPosition()).getPaxListTrip();
        rv.setAdapter(new PaxAdapter(paxs));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setPaxs(ArrayList<PaxListTrip> paxs) {
        this.paxs = paxs;
        init();
    }

    @Override
    public void setLegs(ArrayList<Leg> legs) {
        this.legs = legs;
    }

    @Override
    public void setLegItemData(ArrayList<LegSpinnerItem> items) {
        spinner4.setAdapter(new SpinnerAdapter(items, getContext()));
        new Handler().postDelayed(() -> spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setPaxs(legs.get(i).getPaxListTrip());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        }), 500);

    }

    @Override
    public void shoLoadng() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void initLoading() {

    }

    @Override
    public void showError(String error_trip_detail) {

    }

    @NonNull
    @Override
    public PaxPresenter providePresenter() {
        return presenter;
    }
}
