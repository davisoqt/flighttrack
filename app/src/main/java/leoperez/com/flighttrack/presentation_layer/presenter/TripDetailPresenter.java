package leoperez.com.flighttrack.presentation_layer.presenter;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import net.grandcentrix.thirtyinch.TiConfiguration;
import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.app.FlightApi;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.domain_layer.payload.trip.DataJSON;
import leoperez.com.flighttrack.domain_layer.payload.trip.PayLoadTripDetail;
import leoperez.com.flighttrack.domain_layer.payload.trip.Request;
import leoperez.com.flighttrack.domain_layer.response.trip.TripDetailResponse;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.TripDetailView;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

public class TripDetailPresenter extends TiPresenter<TripDetailView> {

    private static final String TAG = "TripDetail";
    @Inject
    FlightApi retrofit;
    @Inject
    TripsDetailRepository repository;
    @Inject
    SharedPreferences preferences;
    private ArrayList<LegItem> legs;

    public void setTripID(String tripID) {
        this.tripID = tripID;
    }

    String tripID;

    @Override
    protected void onAttachView(@NonNull TripDetailView view) {
        super.onAttachView(view);
        requestData();
    }
    @Inject
    public TripDetailPresenter() {

    }

    public void requestData() {
        getView().load();
        if(legs!=null) {
            getView().showLegs(legs);
            return;
        }
        PayLoadTripDetail payLoadTripDetail = new PayLoadTripDetail();
        payLoadTripDetail.setDataJSON(new DataJSON());
        Request request = new Request();
        payLoadTripDetail.getDataJSON().setRequest(request);
        payLoadTripDetail.getDataJSON().setSessionKey(preferences.getString(Constants.SESSION_KEY, ""));
        payLoadTripDetail.getDataJSON().getRequest().setTripID(tripID);
        if (Constants.DEBUG)
            Log.d(TAG, "request json " + new Gson().toJson(payLoadTripDetail));
        retrofit.getTripDetail(payLoadTripDetail)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tripDetailResponse -> {
                    if (!TextUtils.isEmpty(tripDetailResponse.getFailureMessage()))
                        getView().showError("An error ocurred while triying to load trip detail. "+ "\n " + tripDetailResponse.getFailureMessage());
                    else if(!tripDetailResponse.getReturnData().get(0).getStatus().equals("200")){
                        getView().showError("An error ocurred while triying to load trip detail. "+ "\n " + tripDetailResponse.getReturnData().get(0).getStatusMessage());
                    }
                    else
                        saveAndLoadData(tripDetailResponse);
                }, throwable -> {
                    throwable.printStackTrace();
                    if (Constants.DEBUG) {
                        loadData();
                    }
                    getView().showError("An error ocurred while triying to load trip detail.");
                })
        ;
    }

    private void saveAndLoadData(TripDetailResponse tripDetailResponse) {
        repository.saveTripDetail(tripDetailResponse.getReturnData().get(0).getResult())
                .subscribe(() -> loadData(),throwable -> throwable.printStackTrace());
    }

    private void loadData() {
        repository.getLegsForList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(legItems -> {
                    legs=legItems;
                    getView().showLegs(legItems);},throwable -> throwable.printStackTrace());
    }
}
