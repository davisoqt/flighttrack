package leoperez.com.flighttrack.presentation_layer.view.adapter.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.TripDetail;
import leoperez.com.flighttrack.presentation_layer.view.activity.HomeActivity;
import leoperez.com.flighttrack.presentation_layer.view.activity.TripDetailActivity;
import leoperez.com.flighttrack.presentation_layer.view.fragment.TripDetailFragment;

/**
 * Created by Leo Perez Ortíz on 31/10/17.
 */

public class TrpisAdapter extends RecyclerView.Adapter<TrpisAdapter.ViewHoder> {
    private static final String TAG = "Adapter Home";
    Context context;
    ArrayList<TripDetail> items;


    public TrpisAdapter(Context context, ArrayList<TripDetail> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHoder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.trip_row, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ViewHoder(view);
    }

    @Override
    public void onBindViewHolder(ViewHoder holder, int position) {
        TripDetail item= items.get(position);
        holder.trip.setText("Trip ID: "+ item.getTripID());
        Log.d(TAG, "Trip ID: "+ item.getTripID());
        holder.aircraft.setText("Aircraft ID: "+ item.getAircraftID());
        Log.d(TAG, "Aircraft ID: "+ item.getAircraftID());
        holder.startDate.setText("Start date: "+ item.getStartDate());
        Log.d(TAG, "Start date: "+ item.getStartDate());
        holder.update.setText("Last updated: "+item.getLastUpdated());
        Log.d(TAG, "Last updated: "+item.getLastUpdated());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHoder extends RecyclerView.ViewHolder {
        @BindView(R.id.trip)
        TextView trip;
        @BindView(R.id.start_date)
        TextView startDate;
        @BindView(R.id.aircraft)
        TextView aircraft;
        @BindView(R.id.update)
        TextView update;
        @BindView(R.id.checkout1)
        TextView checkout1;
        @BindView(R.id.checkout2)
        TextView checkout2;

        public ViewHoder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            checkout1.setOnClickListener(view -> {
                TripDetail item= items.get(getAdapterPosition());
                Intent intent= new Intent(context, TripDetailActivity.class);
                FlightApplication.trip_ID=item.getTripID();
                intent.putExtra("ID",item.getTripID());
                intent.putExtra("date",item.getLastUpdated());
                intent.putExtra("start",item.getStartDate());
                intent.putExtra("air",item.getAircraftID());
                context.startActivity(intent);
//                ((HomeActivity)context).gotoSection(TripDetailFragment.newInstance(items.get(getAdapterPosition()).getTripID(),
//                        items.get(getAdapterPosition()).getLastUpdated(),items.get(getAdapterPosition()).getAircraftID()
//                ,items.get(getAdapterPosition()).getStartDate()));
            });
        }
    }
}
