package leoperez.com.flighttrack.presentation_layer.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.Utils;
import com.bumptech.glide.Glide;

import butterknife.BindBool;
import butterknife.BindView;
import butterknife.ButterKnife;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.home.HomeView;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindBool(R.bool.is_tablet)
    boolean is_tablet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Intent i = new Intent(this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        new Handler().postDelayed(() -> {
            startActivity(i);
            finish();
        },2000);
    }
}
