package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.domain_layer.response.trip.PaxListTrip;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public interface PaxView extends BaseView {

    void init();

    void setPaxs(ArrayList<PaxListTrip> paxs);

    void setLegs(ArrayList<Leg> legs);

    void setLegItemData(ArrayList<LegSpinnerItem> items);
}
