package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.google.gson.Gson;

import net.grandcentrix.thirtyinch.TiFragment;
import net.grandcentrix.thirtyinch.distinctuntilchanged.WeakEqualsComparator;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.stream.Stream;

import javax.inject.Inject;

import butterknife.BindBool;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.converter.RowPosition;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.AcCostSave;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.DataJSON;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.Request;
import leoperez.com.flighttrack.domain_layer.response.trip.ACCostArrive;
import leoperez.com.flighttrack.domain_layer.response.trip.ACCostDepart;
import leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.presentation_layer.presenter.ACostPresenter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerAdapter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerCrewAdapter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.ACostView;

public class ACostFragment extends TiFragment<ACostPresenter, ACostView> implements ACostView {

    private static final String TAG = "ACostFragment";
    @BindView(R.id.departure_check)
    RadioButton departureCheck;
    @BindView(R.id.arrive_check)
    RadioButton arriveCheck;
    @BindView(R.id.et_fbo)
    EditText etFbo;
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.et_date)
    EditText etDate;
    @BindView(R.id.spinner_fuel)
    Spinner spinnerFuel;
    @BindView(R.id.fuel_cost)
    EditText fuelCost;
    @BindView(R.id.et_retail)
    EditText et_retail;
    @BindView(R.id.spinner_fuel_cost)
    Spinner spinnerFuelCost;
    Unbinder unbinder;

    @BindView(R.id.spinner4)
    Spinner spinner4;
    @BindView(R.id.fbo_name)
    TextView fboName;
    @BindView(R.id.et_fuel)
    TextView etFuel;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.fuel_unit)
    TextView fuelUnit;
    @BindView(R.id.tv_fuel_cost)
    TextView tvFuelCost;
    @BindView(R.id.tv_retail)
    TextView tvRetail;
    @BindView(R.id.iv_note)
    ImageView ivNote;
    @BindView(R.id.line)
    FrameLayout line;
    private ArrayList<Leg> legs;
    @Inject
    ACostPresenter presenter;
    @Inject
    SharedPreferences preferences;
    //Domain
    ACCostDepart acCostDepart;
    ACCostArrive acCostArrive;
    @BindView(R.id.container)
    LinearLayout container;
    @BindBool(R.bool.is_tablet)
    boolean isTablet;

    private SpinnerCrewAdapter credit_card_adapter;
    private LayoutInflater inflater;
    private SpinnerCrewAdapter fuel_unit_adapter;
    private boolean isOld = false;
    private LinearLayout column1;
    private LinearLayout column2;
    private View current_view_note;
    private String general_note;
    private MaterialDialog mDialog;


    public ACostFragment() {
        // Required empty public constructor
    }

    public static ACostFragment newInstance() {
        ACostFragment fragment = new ACostFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((FlightApplication) getActivity().getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_acost, container, false);
        unbinder = ButterKnife.bind(this, view);
        this.inflater = inflater;
        if (isTablet) {
            column1 = view.findViewById(R.id.c1);
            column2 = view.findViewById(R.id.c2);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth() * 0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth() * 0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lpp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            view.findViewById(R.id.c2_container).setLayoutParams(lp);
            view.findViewById(R.id.c1_container).setLayoutParams(lpp);
        }
        initLoading();
        initCHecks();
        ArrayList<String> units = new ArrayList<String>();
        units.add("SELECT");
        units.add("IMP. GALLONS");
        units.add("LITTERS");
        units.add("U.S.GALLONS");
        fuel_unit_adapter = new SpinnerCrewAdapter(units, getContext());
        spinnerFuel.setAdapter(fuel_unit_adapter);
        return view;
    }

    private void initCHecks() {
        departureCheck.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b)
                initDeparture();
        });
        arriveCheck.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b)
                initArrive();
        });
    }

    private void initDeparture() {
        acCostDepart = legs.get(spinner4.getSelectedItemPosition()).getACCostDepart();
        if (!TextUtils.isEmpty(acCostDepart.getNotes()))
            ivNote.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));
        else
            ivNote.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
        general_note=acCostDepart.getNotes();
        etFbo.setText("");
        editText.setText("");
        etDate.setText("");
        fuelCost.setText("");
        et_retail.setText("");
        if (!TextUtils.isEmpty(acCostDepart.getFBO()))
            etFbo.setText(acCostDepart.getFBO());
        if (!TextUtils.isEmpty(acCostDepart.getQuantity()))
            editText.setText(acCostDepart.getQuantity());
        if (!TextUtils.isEmpty(acCostDepart.getDate()))
            etDate.setText(acCostDepart.getDate());
        if (!TextUtils.isEmpty(acCostDepart.getAmount()))
            fuelCost.setText(acCostDepart.getAmount());
        if (!TextUtils.isEmpty(acCostDepart.getRetailCost()))
            et_retail.setText(acCostDepart.getRetailCost());
        credit_card_adapter = new SpinnerCrewAdapter(FlightApplication.credit_cards, getContext());
        spinnerFuelCost.setAdapter(credit_card_adapter);
        spinnerFuelCost.setSelection(credit_card_adapter.getItemPosition(acCostDepart.getCCard()));
        spinnerFuel.setSelection(fuel_unit_adapter.getItemPosition(acCostDepart.getUnits()));
        if (!isTablet) {
            container.removeAllViews();
            Flowable.create(e -> {
                        for (ACCostFieldList field : acCostDepart.getACCostFieldList()) {
                            View item_view = inflater.inflate(R.layout.a_cost_edit, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.et_value);
                            edit.setHint("Enter " + field.getFieldLabel());
                            if (!TextUtils.isEmpty(field.getAmount()))
                                edit.setText(field.getAmount());
                            ((Spinner) item_view.findViewById(R.id.spinner)).setAdapter(credit_card_adapter);
                            ((Spinner) item_view.findViewById(R.id.spinner)).setSelection(credit_card_adapter.getItemPosition(field.getCCard()));
                            if (TextUtils.isEmpty(field.getNotes()))
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
                            else
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            } else {
                                edit.setEnabled(false);
                                ((Spinner) item_view.findViewById(R.id.spinner)).setEnabled(false);
                            }
                            //notas
                            item_view.setTag(field);
                            item_view.findViewById(R.id.ic_note).setOnClickListener(view -> {
                                current_view_note = view;
                                ACCostFieldList fieldList = (ACCostFieldList) ((LinearLayout) current_view_note.getParent().getParent()).getTag();
                                new MaterialDialog.Builder(getContext())
                                        .title(fieldList.getFieldLabel() + " Notes")
                                        .titleGravity(GravityEnum.CENTER)
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .input("Enter note", fieldList.getNotes(), new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                // Do something
                                                fieldList.setNotes(input.toString());
                                                ((LinearLayout) current_view_note.getParent().getParent()).setTag(fieldList);
                                                if (TextUtils.isEmpty(input))
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.white);
                                                else
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.green);
                                            }
                                        })
                                        .positiveText("Ok")
                                        .buttonsGravity(GravityEnum.CENTER).show();
                            });
                            e.onNext(item_view);
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        container.addView((View) item_view);
                    }, throwable -> throwable.printStackTrace());
        } else {
            column1.removeAllViews();
            column2.removeAllViews();
            final int[] position = {0};
            Flowable.create(e -> {
                        for (ACCostFieldList field : acCostDepart.getACCostFieldList()) {
                            View item_view = inflater.inflate(R.layout.a_cost_edit, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.et_value);
                            edit.setHint("ENTER DATA");
                            if (!TextUtils.isEmpty(field.getAmount()))
                                edit.setText(field.getAmount());
                            ((Spinner) item_view.findViewById(R.id.spinner)).setAdapter(credit_card_adapter);
                            ((Spinner) item_view.findViewById(R.id.spinner)).setSelection(credit_card_adapter.getItemPosition(field.getCCard()));
                            if (TextUtils.isEmpty(field.getNotes()))
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
                            else
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            } else {
                                edit.setEnabled(false);
                                ((Spinner) item_view.findViewById(R.id.spinner)).setEnabled(false);
                            }
                            //notas
                            item_view.setTag(field);
                            item_view.findViewById(R.id.ic_note).setOnClickListener(view -> {
                                current_view_note = view;
                                ACCostFieldList fieldList = (ACCostFieldList) ((LinearLayout) current_view_note.getParent().getParent()).getTag();
                                new MaterialDialog.Builder(getContext())
                                        .title(fieldList.getFieldLabel() + " Notes")
                                        .titleGravity(GravityEnum.CENTER)
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .input("Enter note", fieldList.getNotes(), new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                // Do something
                                                fieldList.setNotes(input.toString());
                                                ((LinearLayout) current_view_note.getParent().getParent()).setTag(fieldList);
                                                if (TextUtils.isEmpty(input))
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.white);
                                                else
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.green);
                                            }
                                        })
                                        .positiveText("Ok")
                                        .buttonsGravity(GravityEnum.CENTER).show();
                            });
                            e.onNext(new RowPosition(position[0]++, item_view));
                            if (position[0] == 2)
                                position[0] = 0;
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        switch (((RowPosition) item_view).getPosition()) {
                            case 0:
                                column1.addView(((RowPosition) item_view).getContent());
                                break;
                            case 1:
                                column2.addView(((RowPosition) item_view).getContent());
                                break;
                        }
                    }, throwable -> throwable.printStackTrace());
        }
    }

    private void initArrive() {
        acCostArrive = legs.get(spinner4.getSelectedItemPosition()).getACCostArrive();
        if (!TextUtils.isEmpty(acCostArrive.getNotes()))
            ivNote.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));
        else
            ivNote.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
        general_note=acCostDepart.getNotes();
        etFbo.setText("");
        editText.setText("");
        etDate.setText("");
        fuelCost.setText("");
        et_retail.setText("");
        if (!TextUtils.isEmpty(acCostArrive.getFBO()))
            etFbo.setText(acCostArrive.getFBO());
        if (!TextUtils.isEmpty(acCostArrive.getQuantity()))
            editText.setText(acCostArrive.getQuantity());
        if (!TextUtils.isEmpty(acCostArrive.getDate()))
            etDate.setText(acCostArrive.getDate());
        if (!TextUtils.isEmpty(acCostArrive.getAmount()))
            fuelCost.setText(acCostArrive.getAmount());
        if (!TextUtils.isEmpty(acCostArrive.getRetailCost()))
            et_retail.setText(acCostArrive.getRetailCost());
        credit_card_adapter = new SpinnerCrewAdapter(FlightApplication.credit_cards, getContext());
        spinnerFuelCost.setAdapter(credit_card_adapter);
        spinnerFuelCost.setSelection(credit_card_adapter.getItemPosition(acCostArrive.getCCard()));
        spinnerFuel.setSelection(fuel_unit_adapter.getItemPosition(acCostArrive.getUnits()));
        if (!isTablet) {
            container.removeAllViews();
            Flowable.create(e -> {
                        for (ACCostFieldList field : acCostArrive.getACCostFieldList()) {
                            View item_view = inflater.inflate(R.layout.a_cost_edit, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.et_value);
                            edit.setHint("Enter " + field.getFieldLabel());
                            if (!TextUtils.isEmpty(field.getAmount()))
                                edit.setText(field.getAmount());
                            ((Spinner) item_view.findViewById(R.id.spinner)).setAdapter(credit_card_adapter);
                            ((Spinner) item_view.findViewById(R.id.spinner)).setSelection(credit_card_adapter.getItemPosition(field.getCCard()));
                            if (TextUtils.isEmpty(field.getNotes()))
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
                            else
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    KeyboardUtils.showSoftInput(((EditText) view.getTag()));
                                });
                            } else {
                                edit.setEnabled(false);
                                ((Spinner) item_view.findViewById(R.id.spinner)).setEnabled(false);
                            }
                            //notas
                            item_view.setTag(field);
                            item_view.findViewById(R.id.ic_note).setOnClickListener(view -> {
                                current_view_note = view;
                                ACCostFieldList fieldList = (ACCostFieldList) ((LinearLayout) current_view_note.getParent().getParent()).getTag();
                                new MaterialDialog.Builder(getContext())
                                        .title(fieldList.getFieldLabel() + " Notes")
                                        .titleGravity(GravityEnum.CENTER)
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .input("Enter note", fieldList.getNotes(), new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                // Do something
                                                fieldList.setNotes(input.toString());
                                                ((LinearLayout) current_view_note.getParent().getParent()).setTag(fieldList);
                                                if (TextUtils.isEmpty(input))
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.white);
                                                else
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.green);
                                            }
                                        })
                                        .positiveText("Ok")
                                        .buttonsGravity(GravityEnum.CENTER).show();
                            });
                            e.onNext(item_view);
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        container.addView((View) item_view);
                    }, throwable -> throwable.printStackTrace());
        } else {
            column1.removeAllViews();
            column2.removeAllViews();
            final int[] position = {0};
            Flowable.create(e -> {
                        for (ACCostFieldList field : acCostArrive.getACCostFieldList()) {
                            View item_view = inflater.inflate(R.layout.a_cost_edit, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.et_value);
                            edit.setHint("Enter " + field.getFieldLabel());
                            if (!TextUtils.isEmpty(field.getAmount()))
                                edit.setText(field.getAmount());
                            ((Spinner) item_view.findViewById(R.id.spinner)).setAdapter(credit_card_adapter);
                            ((Spinner) item_view.findViewById(R.id.spinner)).setSelection(credit_card_adapter.getItemPosition(field.getCCard()));
                            if (TextUtils.isEmpty(field.getNotes()))
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
                            else
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            } else {
                                edit.setEnabled(false);
                                ((Spinner) item_view.findViewById(R.id.spinner)).setEnabled(false);
                            }
                            //notes
                            item_view.setTag(field);
                            item_view.findViewById(R.id.ic_note).setOnClickListener(view -> {
                                current_view_note = view;
                                ACCostFieldList fieldList = (ACCostFieldList) ((LinearLayout) current_view_note.getParent().getParent()).getTag();
                                new MaterialDialog.Builder(getContext())
                                        .title(fieldList.getFieldLabel() + " Notes")
                                        .titleGravity(GravityEnum.CENTER)
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .input("Enter note", fieldList.getNotes(), new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                // Do something
                                                fieldList.setNotes(input.toString());
                                                ((LinearLayout) current_view_note.getParent().getParent()).setTag(fieldList);
                                                if (TextUtils.isEmpty(input))
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.white);
                                                else
                                                    ((ImageView) current_view_note.findViewById(R.id.ic_note)).setImageResource(R.drawable.green);
                                            }
                                        })
                                        .positiveText("Ok")
                                        .buttonsGravity(GravityEnum.CENTER).show();
                            });
                            e.onNext(new RowPosition(position[0]++, item_view));
                            if (position[0] == 2)
                                position[0] = 0;
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        switch (((RowPosition) item_view).getPosition()) {
                            case 0:
                                column1.addView(((RowPosition) item_view).getContent());
                                break;
                            case 1:
                                column2.addView(((RowPosition) item_view).getContent());
                                break;
                        }
                    }, throwable -> throwable.printStackTrace());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.et_date)
    public void onViewClicked() {
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener((dialog, year, monthOfYear, dayOfMonth) -> {
                    String y = (year + "").substring(2);
                    etDate.setText(monthOfYear + "/" + dayOfMonth + "/" + y);
                })
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setDoneText("Ok")
                .setCancelText("Cancel");
        cdp.show(getChildFragmentManager(), "s");
    }

    public void reload() {
        if (arriveCheck.isChecked())
            initArrive();
        else
            initDeparture();
    }

    @Override
    public void shoLoadng() {
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void initLoading() {
        mDialog= new MaterialDialog.Builder(getContext())
                .progress(true,30)
                .content("Loading...")
                .build();
    }

    @Override
    public void showError(String error_trip_detail) {
        hideLoading();
        new MaterialDialog.Builder(getContext())
                .title("Error")
                .content(error_trip_detail)
                .negativeText("Retry")
                .onNegative((dialog, which) -> onViewClickedSave())
                .positiveText("Cancel")
                .show();
    }


    @Override
    public void showCodeError(String s) {
        hideLoading();
        new MaterialDialog.Builder(getContext())
                .title("Error")
                .titleGravity(GravityEnum.CENTER)
                .content(s)
                .neutralText("Ok")
                .buttonsGravity(GravityEnum.CENTER)
                .show();
    }

    @Override
    public void showSucces() {
        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .content("AcCost data updated successfully!")
                .buttonsGravity(GravityEnum.CENTER)
                .neutralText("Ok")
                .show();
    }

    @Override
    public void setLegs(ArrayList<Leg> legs) {
        this.legs = legs;
    }

    @Override
    public void setLegItemData(ArrayList<LegSpinnerItem> items) {
        spinner4.setAdapter(new SpinnerAdapter(items, getContext()));
        new Handler().postDelayed(() -> spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reload();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        }), 500);
        if (isOld) {
            arriveCheck.setEnabled(false);
            departureCheck.setEnabled(false);
            etFbo.setEnabled(false);
            fuelCost.setEnabled(false);
            etDate.setEnabled(false);
            editText.setEnabled(false);
            spinnerFuel.setEnabled(false);
            spinnerFuelCost.setEnabled(false);
            et_retail.setEnabled(false);
        } else {
            fboName.setOnClickListener(view -> KeyboardUtils.showSoftInput(etFbo));
            etFuel.setOnClickListener(view -> KeyboardUtils.showSoftInput(editText));
            tvDate.setOnClickListener(view -> tvDate.performLongClick());
            tvFuelCost.setOnClickListener(view -> KeyboardUtils.showSoftInput(fuelCost));
            tvRetail.setOnClickListener(view -> KeyboardUtils.showSoftInput(et_retail));
        }
        initDeparture();
    }

    @Override
    public void isOld(boolean old) {
        this.isOld = old;
    }

    @NonNull
    @Override
    public ACostPresenter providePresenter() {
        return presenter;
    }

    @OnClick(R.id.iv_note)
    public void genralNote() {
        new MaterialDialog.Builder(getContext())
                .title("Note")
                .titleGravity(GravityEnum.CENTER)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("Enter note", general_note, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        general_note= input.toString();
                        if (TextUtils.isEmpty(input))
                            ivNote.setImageResource(R.drawable.white);
                        else
                            ivNote.setImageResource(R.drawable.green);
                    }
                })
                .positiveText("Ok")
                .buttonsGravity(GravityEnum.CENTER).show();
    }

    @OnClick(R.id.save)
    public void onViewClickedSave() {
        shoLoadng();
        ArrayList<ACCostFieldList> fieldLists= new ArrayList<>();
        io.reactivex.Observable.create(e -> {
            AcCostSave acCostSave= new AcCostSave();
            DataJSON dataJSON= new DataJSON();
            Request request= new Request();
            //session id
            dataJSON.setSessionKey(preferences.getString(Constants.SESSION_KEY,""));
            acCostSave.setDataJSON(dataJSON);
            dataJSON.setRequest(request);
            //static field
            request.setNotes(general_note);
            if(departureCheck.isChecked())
                request.setADFlag("D");
            else
            request.setADFlag("A");
            request.setTripID(FlightApplication.trip_ID);
            request.setFBO(etFbo.getText().toString());
            request.setQuantity(editText.getText().toString());
            request.setCostDate(etDate.getText().toString());
            request.setUnits(((String) spinnerFuel.getSelectedItem()).toLowerCase().contains("selec") ? "":((String)spinnerFuel.getSelectedItem()));
            request.setAmount(fuelCost.getText().toString());
            request.setCCard(((String)spinnerFuelCost.getSelectedItem()).toLowerCase().contains("selec") ? "":((String)spinnerFuelCost.getSelectedItem()));
            request.setRetailCost(et_retail.getText().toString());
            request.setLegNumber(legs.get(spinner4.getSelectedItemPosition()).getLegNumber());
            if(!isTablet) {
                for (int i = 0; i < container.getChildCount(); i++) {
                    View view = container.getChildAt(i);
                    ACCostFieldList item = (ACCostFieldList) container.getChildAt(i).getTag();
                    String value = ((EditText) view.findViewById(R.id.et_value)).getText() == null ? "" : ((EditText) view.findViewById(R.id.et_value)).getText().toString();
                    item.setAmount(value == null ? "" : value);
                    String card = (String) ((Spinner) view.findViewById(R.id.spinner)).getSelectedItem();
                    if (card.toLowerCase().contains("selec"))
                        item.setCCard("");
                    else
                        item.setCCard(card);
                    fieldLists.add(item);
                }
            }
            else {
                for (int i = 0; i < column1.getChildCount(); i++) {
                    View view = column1.getChildAt(i);
                    ACCostFieldList item = (ACCostFieldList) column1.getChildAt(i).getTag();
                    String value = ((EditText) view.findViewById(R.id.et_value)).getText() == null ? "" : ((EditText) view.findViewById(R.id.et_value)).getText().toString();
                    item.setAmount(value == null ? "" : value);
                    String card = (String) ((Spinner) view.findViewById(R.id.spinner)).getSelectedItem();
                    if (card.toLowerCase().contains("selec"))
                        item.setCCard("");
                    else
                        item.setCCard(card);
                    fieldLists.add(item);
                }
                for (int i = 0; i < column2.getChildCount(); i++) {
                    View view = column2.getChildAt(i);
                    ACCostFieldList item = (ACCostFieldList) column2.getChildAt(i).getTag();
                    String value = ((EditText) view.findViewById(R.id.et_value)).getText() == null ? "" : ((EditText) view.findViewById(R.id.et_value)).getText().toString();
                    item.setAmount(value == null ? "" : value);
                    String card = (String) ((Spinner) view.findViewById(R.id.spinner)).getSelectedItem();
                    if (card.toLowerCase().contains("selec"))
                        item.setCCard("");
                    else
                        item.setCCard(card);
                    fieldLists.add(item);
                }
            }
            request.setACCostFieldList(fieldLists);
            if(Constants.DEBUG)
                Log.d(TAG, "ACCOST_SAVE"+ new Gson().toJson(acCostSave));
            e.onNext(acCostSave);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                        presenter.saveAcCOst((AcCostSave) o);
                },throwable -> throwable.printStackTrace());
    }
}
