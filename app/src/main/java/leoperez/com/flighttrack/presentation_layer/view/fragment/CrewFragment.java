package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.google.gson.Gson;

import net.grandcentrix.thirtyinch.TiFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindBool;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.converter.RowPosition;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.AcCostSave;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.DataJSON;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.Request;
import leoperez.com.flighttrack.domain_layer.payload.save_crew.SaveCrew;
import leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList;
import leoperez.com.flighttrack.domain_layer.response.trip.Crew;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.presentation_layer.presenter.CrewPresenter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerAdapter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerCrewAdapter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.CrewFragmentView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CrewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CrewFragment extends TiFragment<CrewPresenter, CrewFragmentView> implements CrewFragmentView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "CrewFragment";
    @BindView(R.id.container)
    LinearLayout container;
    Unbinder unbinder;
    @BindView(R.id.spinner2)
    Spinner spinner_pic;
    @BindView(R.id.check_pic)
    CheckBox checkPic;
    @BindView(R.id.spinner3)
    Spinner spinner_sic;
    @BindView(R.id.check_sic)
    CheckBox checkSic;

    //Domain object
    Crew crew;
    ArrayList<String> crew_ids;
    MaterialDialog materialDialog;
    @BindView(R.id.spinner4)
    Spinner spinner4;
    @BindView(R.id.spinner_acm1)
    Spinner spinnerAcm1;
    @BindView(R.id.spinner_acm2)
    Spinner spinnerAcm2;
    @BindView(R.id.spinner_acm3)
    Spinner spinnerAcm3;
    @BindView(R.id.spinner_acm4)
    Spinner spinnerAcm4;
    @BindBool(R.bool.is_tablet)
    boolean isTablet;
    private LayoutInflater inflater;
    private SpinnerCrewAdapter pic_adapter;
    private SpinnerCrewAdapter sic_adapter;
    private SpinnerCrewAdapter acm1_adapert;
    private SpinnerCrewAdapter acm1_adaper2;
    private SpinnerCrewAdapter acm1_adaper3;
    private SpinnerCrewAdapter acm1_adaper4;
    private View selected_view;
    private ArrayList<Leg> legs;
    @Inject
    CrewPresenter presenter;
    @Inject
    SharedPreferences preferences;
    private boolean isOld;
    private LinearLayout column1;
    private LinearLayout column2;

    public CrewFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CrewFragment newInstance(ArrayList<String> crew_ids) {
        CrewFragment fragment = new CrewFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM2, crew_ids);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((FlightApplication) getActivity().getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.crew_ids = (ArrayList<String>) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_crew, container, false);
        this.inflater = inflater;
        unbinder = ButterKnife.bind(this, view);
        initLoading();
        if (isTablet) {
            column1 = view.findViewById(R.id.c1);
            column2 = view.findViewById(R.id.c2);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth() * 0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth() * 0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lpp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            view.findViewById(R.id.c2_container).setLayoutParams(lp);
            view.findViewById(R.id.c1_container).setLayoutParams(lpp);
        }
        return view;
    }

    private void init() {
        crew= legs.get(spinner4.getSelectedItemPosition()).getCrew();
        spinnerAcm1.setSelection(acm1_adapert.getItemPosition(crew.getACM1Id()));
        spinnerAcm2.setSelection(acm1_adaper2.getItemPosition(crew.getACM2Id()));
        spinnerAcm3.setSelection(acm1_adaper3.getItemPosition(crew.getACM3Id()));
        spinnerAcm4.setSelection(acm1_adaper4.getItemPosition(crew.getACM4Id()));
        ArrayList<String> items_pic = new ArrayList<>();
        items_pic.add("Select PIC");
        items_pic.addAll(crew_ids);
        ArrayList<String> items_sic = new ArrayList<>();
        items_sic.add("Select SIC");
        items_sic.addAll(crew_ids);
        pic_adapter = new SpinnerCrewAdapter(items_pic, getContext());
        spinner_pic.setAdapter(pic_adapter);
        spinner_pic.setSelection(pic_adapter.getItemPosition(crew.getPICId()));
        sic_adapter = new SpinnerCrewAdapter(items_sic, getContext());
        spinner_sic.setAdapter(sic_adapter);
        spinner_sic.setSelection(sic_adapter.getItemPosition(crew.getSICId()));
        if (!TextUtils.isEmpty(crew.getPICFlying()) && crew.getPICFlying().equals("true"))
            checkPic.setChecked(true);
        else
            checkPic.setChecked(false);
        if (!TextUtils.isEmpty(crew.getSICFlying()) && crew.getSICFlying().equals("true"))
            checkSic.setChecked(true);
        else
            checkSic.setChecked(false);
        if (isOld) {
            spinnerAcm1.setEnabled(false);
            spinnerAcm2.setEnabled(false);
            spinnerAcm3.setEnabled(false);
            spinnerAcm4.setEnabled(false);
            spinner4.setEnabled(false);
            spinner_pic.setEnabled(false);
            spinner_sic.setEnabled(false);
            checkSic.setEnabled(false);
            checkPic.setEnabled(false);
        }
        if (!isTablet) {
            container.removeAllViews();
            Flowable.create(e -> {
                        for (CrewFieldList field : crew.getCrewFieldList()
                                ) {
                            //no hay descrpcion lo omito
                            if (TextUtils.isEmpty(field.getFieldLabel()))
                                continue;
                            View item_view = inflater.inflate(R.layout.edittext_item, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.editText);
                            edit.setHint("Enter " + field.getFieldLabel());
                            if (isOld)
                                edit.setEnabled(false);
                            if (!TextUtils.isEmpty(field.getFieldValue()))
                                edit.setText(field.getFieldValue());
                            //InputType
                            Log.d(TAG, "init: fielType " + field.getFieldType());
                            if (field.getFieldType().equals("A"))
                                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                            if (field.getFieldType().equals("I"))
                                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                            if (field.getFieldType().equals("T")) {
                                edit.setFocusable(false);
                                edit.setFocusableInTouchMode(false);
                                edit.setOnClickListener(view -> {
                                    selected_view = view;
                                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                                            .setStartTime(10, 10)
                                            .setOnTimeSetListener((dialog, hourOfDay, minute) -> {
                                                ((EditText) selected_view).setText(hourOfDay + ":" + minute);
                                            })
                                            .setDoneText("Ok")
                                            .setCancelText("Cancel");
                                    rtpd.show(getChildFragmentManager(), "d");
                                });
                            }
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    if (field.getFieldType().equals("T"))
                                        ((EditText) view.getTag()).performClick();
                                    else
                                        KeyboardUtils.showSoftInput(((EditText) view.getTag()));
                                });
                            }
                            item_view.setTag(field);
                            e.onNext(item_view);
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        container.addView((View) item_view);
                    }, throwable -> throwable.printStackTrace(), () -> materialDialog.dismiss());
        } else {
            final int[] position = {0};
            column1.removeAllViews();
            column2.removeAllViews();
            Flowable.create(e -> {
                        for (CrewFieldList field : crew.getCrewFieldList()) {
                            //no hay descrpcion lo omito
                            if (TextUtils.isEmpty(field.getFieldLabel()))
                                continue;
                            View item_view = inflater.inflate(R.layout.edittext_item, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.editText);
                            edit.setHint("ENTER DATA ");
                            if (!TextUtils.isEmpty(field.getFieldValue()))
                                edit.setText(field.getFieldValue());
                            //InputType
                            Log.d(TAG, "init: fielType " + field.getFieldType());
                            if (field.getFieldType().equals("A"))
                                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                            if (field.getFieldType().equals("I"))
                                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                            if (field.getFieldType().equals("T")) {
                                edit.setFocusable(false);
                                edit.setFocusableInTouchMode(false);
                                edit.setOnClickListener(view -> {
                                    selected_view = view;
                                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                                            .setStartTime(10, 10)
                                            .setOnTimeSetListener((dialog, hourOfDay, minute) -> {
                                                ((EditText) selected_view).setText(hourOfDay + ":" + minute);
                                            })
                                            .setDoneText("Ok")
                                            .setCancelText("Cancel");
                                    rtpd.show(getChildFragmentManager(), "d");
                                });
                            }
                            //click label
                            if (!isOld) {
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    if (field.getFieldType().equals("T"))
                                        ((EditText) view.getTag()).performClick();
                                    else
                                        KeyboardUtils.showSoftInput(((EditText) view.getTag()));
                                });
                            } else {
                                edit.setEnabled(false);
                            }
                            item_view.setTag(field);
                            e.onNext(new RowPosition(position[0]++, item_view));
                            if (position[0] == 2)
                                position[0] = 0;
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        switch (((RowPosition) item_view).getPosition()) {
                            case 0:
                                column1.addView(((RowPosition) item_view).getContent());
                                break;
                            case 1:
                                column2.addView(((RowPosition) item_view).getContent());
                                break;
                        }
                    }, throwable -> throwable.printStackTrace());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @NonNull
    @Override
    public CrewPresenter providePresenter() {
        return presenter;
    }

    public void setCrew(Crew crew) {
        this.crew = crew;
        init();
    }

    @Override
    public void shoLoadng() {
        materialDialog.show();
    }

    @Override
    public void hideLoading() {
        materialDialog.dismiss();
    }


    @Override
    public void initLoading() {
        materialDialog = new MaterialDialog.Builder(getContext()).progress(true, 30)
                .cancelable(false).content("Loading...").build();
    }

    @Override
    public void showError(String error_trip_detail) {

    }

    @Override
    public void showCodeError(String s) {
        materialDialog.dismiss();;
        new MaterialDialog.Builder(getContext())
                .title("Error")
                .titleGravity(GravityEnum.CENTER)
                .content(s)
                .neutralText("Ok")
                .buttonsGravity(GravityEnum.CENTER)
                .show();
    }

    @Override
    public void showSucces() {
        new MaterialDialog.Builder(getContext())
                .title("Info")
                .titleGravity(GravityEnum.CENTER)
                .contentGravity(GravityEnum.CENTER)
                .content("Crew data updated successfully!")
                .buttonsGravity(GravityEnum.CENTER)
                .neutralText("Ok")
                .show();
    }

    @Override
    public void setLegs(ArrayList<Leg> legs) {
        this.legs = legs;
    }

    @Override
    public void setLegItemData(ArrayList<LegSpinnerItem> items) {
        spinner4.setAdapter(new SpinnerAdapter(items, getContext()));
        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setCrew(legs.get(i).getCrew());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void initViews() {
        init();
    }

    @Override
    public void initACM(ArrayList<String> amc) {
        ArrayList<String> ac1 = new ArrayList<>();
        ac1.add("Select ACM1");
        ac1.addAll(amc);
        ArrayList<String> ac2 = new ArrayList<>();
        ac2.add("Select ACM2");
        ac2.addAll(amc);
        ArrayList<String> ac3 = new ArrayList<>();
        ac3.add("Select ACM3");
        ac3.addAll(amc);
        ArrayList<String> ac4 = new ArrayList<>();
        ac4.add("Select ACM4");
        ac4.addAll(amc);
        acm1_adapert = new SpinnerCrewAdapter(ac1, getContext());
        acm1_adaper2 = new SpinnerCrewAdapter(ac2, getContext());
        acm1_adaper3 = new SpinnerCrewAdapter(ac3, getContext());
        acm1_adaper4 = new SpinnerCrewAdapter(ac4, getContext());
        spinnerAcm1.setAdapter(acm1_adapert);
        spinnerAcm2.setAdapter(acm1_adaper2);
        spinnerAcm3.setAdapter(acm1_adaper3);
        spinnerAcm4.setAdapter(acm1_adaper4);

    }

    @Override
    public void isOld(boolean old) {
        isOld = old;
    }

    @OnClick(R.id.textView23)
    public void onViewClicked() {
        shoLoadng();
        ArrayList<CrewFieldList> fieldLists= new ArrayList<>();
        io.reactivex.Observable.create(e -> {
            SaveCrew save_crew= new SaveCrew();
            leoperez.com.flighttrack.domain_layer.payload.save_crew.DataJSON dataJSON= new leoperez.com.flighttrack.domain_layer.payload.save_crew.DataJSON();
            leoperez.com.flighttrack.domain_layer.payload.save_crew.Request request= new leoperez.com.flighttrack.domain_layer.payload.save_crew.Request();
            //session id
            dataJSON.setSessionKey(preferences.getString(Constants.SESSION_KEY,""));
            save_crew.setDataJSON(dataJSON);
            dataJSON.setRequest(request);
            request.setTripID(FlightApplication.trip_ID);
            request.setLegNumber(legs.get(spinner4.getSelectedItemPosition()).getLegNumber());
            //static field
            leoperez.com.flighttrack.domain_layer.payload.save_crew.Crew pay_load_crew= new leoperez.com.flighttrack.domain_layer.payload.save_crew.Crew();
            pay_load_crew.setACM1Id(((String)spinnerAcm1.getSelectedItem()).toLowerCase().contains("select") ? "": ((String)spinnerAcm1.getSelectedItem()));
            pay_load_crew.setACM2Id(((String)spinnerAcm2.getSelectedItem()).toLowerCase().contains("select") ? "": ((String)spinnerAcm2.getSelectedItem()));
            pay_load_crew.setACM3Id(((String)spinnerAcm3.getSelectedItem()).toLowerCase().contains("select") ? "": ((String)spinnerAcm3.getSelectedItem()));
            pay_load_crew.setACM4Id(((String)spinnerAcm4.getSelectedItem()).toLowerCase().contains("select") ? "": ((String)spinnerAcm4.getSelectedItem()));
            if(checkPic.isChecked())
                pay_load_crew.setPICFlying("true");
            else
                pay_load_crew.setPICFlying("false");
            if(checkSic.isChecked())
                pay_load_crew.setSICFlying("true");
            else
                pay_load_crew.setSICFlying("false");
            pay_load_crew.setPICId((((String)spinner_pic.getSelectedItem()).toLowerCase().contains("select") ? "": ((String)spinner_pic.getSelectedItem())));
            pay_load_crew.setSICId((((String)spinner_sic.getSelectedItem()).toLowerCase().contains("select") ? "": ((String)spinner_sic.getSelectedItem())));
            if(!isTablet) {
                for (int i = 0; i < container.getChildCount(); i++) {
                    View vieww = container.getChildAt(i);
                    CrewFieldList item = (CrewFieldList) container.getChildAt(i).getTag();
                    EditText editText=((EditText) vieww.findViewById(R.id.editText));
                    String value = editText.getText() == null ? "" : editText.getText().toString();
                    item.setFieldValue(value == null ? "" : value);
                    fieldLists.add(item);
                }
            }
            else {
                for (int i = 0; i < column1.getChildCount(); i++) {
                    View vieww = column1.getChildAt(i);
                    CrewFieldList item = (CrewFieldList) column1.getChildAt(i).getTag();
                    EditText editText=((EditText) vieww.findViewById(R.id.editText));
                    String value = editText.getText() == null ? "" : editText.getText().toString();
                    item.setFieldValue(value == null ? "" : value);
                    fieldLists.add(item);
                }
                for (int i = 0; i < column2.getChildCount(); i++) {
                    View vieww = column2.getChildAt(i);
                    CrewFieldList item = (CrewFieldList) column2.getChildAt(i).getTag();
                    EditText editText=((EditText) vieww.findViewById(R.id.editText));
                    String value = editText.getText() == null ? "" : editText.getText().toString();
                    item.setFieldValue(value == null ? "" : value);
                    fieldLists.add(item);
                }
            }
            request.setCrewFieldList(fieldLists);
            request.setCrew(pay_load_crew);
            if(Constants.DEBUG)
                Log.d(TAG, "CREW_SAVE "+ new Gson().toJson(save_crew));
            e.onNext(save_crew);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    presenter.saveCrew((SaveCrew) o);
                },throwable -> throwable.printStackTrace());
    }
}
