package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import net.grandcentrix.thirtyinch.TiView;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;

/**
 * Created by Leo Perez Ortíz on 5/11/17.
 */

public interface CrewFragmentView extends BaseView {

    void setLegs(ArrayList<Leg> legs);

    void setLegItemData(ArrayList<LegSpinnerItem> items);

    void initViews();

    void initACM(ArrayList<String> amc);

    void isOld(boolean old);

    void showCodeError(String s);

    void showSucces();
}
