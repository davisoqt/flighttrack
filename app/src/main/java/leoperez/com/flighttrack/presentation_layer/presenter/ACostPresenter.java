package leoperez.com.flighttrack.presentation_layer.presenter;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;

import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.app.FlightApi;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.converter.ConverterModel;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.payload.save_accost.AcCostSave;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.TripDetail;
import leoperez.com.flighttrack.domain_layer.response.save_accost.ReturnDatum;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.ACostView;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public class ACostPresenter extends TiPresenter<ACostView> {

    @Inject
    TripsDetailRepository repository;
    @Inject
    FlightApi retrofit;

    @Inject
    public ACostPresenter() {
    }

    @Override
    protected void onAttachView(@NonNull ACostView view) {
        super.onAttachView(view);
        init();
    }

    private void init() {
        repository.getLegs()
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().setLegs((ArrayList<Leg>) o);
                    initLegs((ArrayList<Leg>) o);
                }, throwable -> {
                    ((Throwable) throwable).printStackTrace();
                });
    }

    private void initLegs(ArrayList<Leg> legs) {
        Observable.create(e -> {
            ArrayList<LegSpinnerItem> data = new ArrayList<>();
            Stream.of(legs).forEach(leg ->
                    data.add(ConverterModel.getLegsSpinner(leg)));
            e.onNext(data);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().isOld(repository.isOld());
                    getView().setLegItemData((ArrayList<LegSpinnerItem>) o);
                });
    }

    public void saveAcCOst(AcCostSave acCostSave) {
        retrofit.saveAcCost(acCostSave)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(saveAcCostResponse -> {
                    if (saveAcCostResponse.getReturnCode().equals("true") && saveAcCostResponse.getReturnData()
                            .get(0).getStatus().equals("200"))
                        saveLocal(saveAcCostResponse.getReturnData().get(0));
                    else if(saveAcCostResponse.getReturnData()!= null && saveAcCostResponse.getReturnData().size()>0)
                        getView().showCodeError(saveAcCostResponse.getReturnData().get(0).getStatusMessage());
                    else
                        getView().showCodeError(saveAcCostResponse.getFailureMessage());

                }, throwable -> {
                    throwable.printStackTrace();
                    getView().showError("There was a problem trying to update the data.");
                });
    }

    private void saveLocal(ReturnDatum returnDatum) {
        Observable.create(e -> {
            ArrayList<Leg> legs = (ArrayList<Leg>) repository.updateLeg(returnDatum);
            getView().setLegs(legs);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                }, throwable -> throwable.printStackTrace(), () -> {getView().hideLoading();
                getView().showSucces();});
    }
}
