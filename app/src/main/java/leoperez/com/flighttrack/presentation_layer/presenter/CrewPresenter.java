package leoperez.com.flighttrack.presentation_layer.presenter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;

import com.annimon.stream.Stream;
import com.blankj.utilcode.util.SizeUtils;

import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.app.FlightApi;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.data_layer.repository.MasterListRepository;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.converter.ConverterModel;
import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.payload.save_crew.Request;
import leoperez.com.flighttrack.domain_layer.payload.save_crew.SaveCrew;
import leoperez.com.flighttrack.domain_layer.response.save_accost.ReturnDatum;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.presentation_layer.view.fragment.ACostFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.PaxFragment;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.CrewFragmentView;

/**
 * Created by Leo Perez Ortíz on 5/11/17.
 */

public class CrewPresenter extends TiPresenter<CrewFragmentView> {
    @Inject
    TripsDetailRepository repository;
    @Inject
    MasterListRepository masterListRepository;
    @Inject
    FlightApi retrofit;

    @Inject
    public CrewPresenter() {
    }

    @Override
    public void attachView(@NonNull CrewFragmentView view) {
        super.attachView(view);
        init();
    }

    private void init() {
        repository.getLegs()
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().setLegs((ArrayList<Leg>) o);
                    initLegs((ArrayList<Leg>) o);
                }, throwable -> {
                    ((Throwable) throwable).printStackTrace();
                });
    }

    private void initLegs(ArrayList<Leg> legs) {
        Observable.create(e -> {
            ArrayList<LegSpinnerItem> data = new ArrayList<>();
            Stream.of(legs).forEach(leg ->
                    data.add(ConverterModel.getLegsSpinner(leg)));
            e.onNext(data);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                   getView().setLegItemData((ArrayList<LegSpinnerItem>) o);
                   getView().initACM(masterListRepository.getAMC());
                   getView().isOld(repository.isOld());
                   getView().initViews();
                });
    }

    public void saveCrew(SaveCrew o) {
        retrofit.saveCrew(o)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(saveAcCostResponse -> {
                    if (saveAcCostResponse.getReturnCode().equals("true") && saveAcCostResponse.getReturnData()
                            .get(0).getStatus().equals("200"))
                        saveLocal(saveAcCostResponse.getReturnData().get(0).getResult().getRequest());
                    else if(saveAcCostResponse.getReturnData()!= null && saveAcCostResponse.getReturnData().size()>0)
                        getView().showCodeError(saveAcCostResponse.getReturnData().get(0).getStatusMessage());
                    else
                        getView().showCodeError(saveAcCostResponse.getFailureMessage());

                }, throwable -> {
                    throwable.printStackTrace();
                    getView().showError("There was a problem trying to update the data.");
                });
    }

    private void saveLocal(Request returnDatum) {
        Observable.create(e -> {
            ArrayList<Leg> legs = (ArrayList<Leg>) repository.updateLeg(returnDatum);
            getView().setLegs(legs);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                }, throwable -> throwable.printStackTrace(), () -> {getView().hideLoading();
                getView().showSucces();});
    }


}
