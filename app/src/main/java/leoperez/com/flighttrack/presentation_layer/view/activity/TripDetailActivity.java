package leoperez.com.flighttrack.presentation_layer.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Stream;
import com.blankj.utilcode.util.SizeUtils;

import net.grandcentrix.thirtyinch.TiActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.domain_layer.converter.RowPosition;
import leoperez.com.flighttrack.presentation_layer.presenter.TripDetailPresenter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.TripDetailView;

public class TripDetailActivity extends TiActivity<TripDetailPresenter, TripDetailView> implements TripDetailView {
    @BindView(R.id.leg_container)
    LinearLayout legContainer;
    @BindView(R.id.plane1_container)
    LinearLayout plane1Container;
    @BindView(R.id.plane2_container)
    LinearLayout plane2Container;
    @BindView(R.id.check_container)
    LinearLayout checkContainer;
    Unbinder unbinder;
    @BindView(R.id.trip)
    TextView trip;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.craft_id)
    TextView craftId;
    @BindView(R.id.update)
    TextView update;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private LayoutInflater inflater;
    private MaterialDialog mDialog;
    @Inject
    TripDetailPresenter presenter;

    ArrayList<WeakReference<CheckBox>> views_checks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((FlightApplication) getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_trip_detail);
        ButterKnife.bind(this);
        initLoading();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this,R.drawable.ic_action_back));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initLoading();
        presenter.setTripID(getIntent().getStringExtra("ID"));
        trip.setText("Trip ID: " + getIntent().getStringExtra("ID"));
        date.setText("Start date: " + getIntent().getStringExtra("start"));
        craftId.setText("AirCraft ID: " + getIntent().getStringExtra("air"));
        update.setText("Last Update: " + getIntent().getStringExtra("date"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public TripDetailPresenter providePresenter() {
        return presenter;
    }

    @Override
    public void shoLoadng() {
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void initLoading() {
        mDialog = new MaterialDialog.Builder(this)
                .content("Loading...").progress(true, 30).build();
    }

    @Override
    public void showError(String error_trip_detail) {
        new MaterialDialog.Builder(this)
                .title("Error")
                .content(error_trip_detail)
                .negativeText("Retry")
                .onNegative((dialog, which) -> presenter.requestData())
                .positiveText("Cancel")
                .show();
    }

    @Override
    public void load() {
        shoLoadng();
    }

    @Override
    public void showLegs(ArrayList<LegItem> legs) {
        inflater = LayoutInflater.from(this);
        final int[] flag = {0};
        legContainer.removeAllViews();
        plane1Container.removeAllViews();
        plane2Container.removeAllViews();
        checkContainer.removeAllViews();
        Flowable.create(e -> {
            Stream.of(legs).forEach(legItems -> {
                View leg_view = inflater.inflate(R.layout.trip_detail_leg, null);
                ((TextView) leg_view.findViewById(R.id.textView9)).setText(legItems.getID());
                e.onNext(new RowPosition(0, leg_view));
                FrameLayout frameLayout= new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(1)));
                frameLayout.setBackgroundColor(Color.parseColor("#c4c4c4"));
                //divider
                e.onNext(new RowPosition(0, frameLayout));
                //change leg ID
                View depart_view = inflater.inflate(R.layout.trip_depart, null);
                ((TextView) depart_view.findViewById(R.id.textView10)).setText(legItems.getDepart());
                e.onNext(new RowPosition(1, depart_view));
                //divider
                frameLayout= new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(1)));
                frameLayout.setBackgroundColor(Color.parseColor("#c4c4c4"));
                e.onNext(new RowPosition(1, frameLayout));
                //change depart
                View arriw_view = inflater.inflate(R.layout.trip_arrive, null);
                ((TextView) arriw_view.findViewById(R.id.textView10)).setText(legItems.getArriv());
                e.onNext(new RowPosition(2, arriw_view));
                //divider
                frameLayout= new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(1)));
                frameLayout.setBackgroundColor(Color.parseColor("#c4c4c4"));
                e.onNext(new RowPosition(2, frameLayout));
                //change arrive
                View check_view = inflater.inflate(R.layout.trip_check_out, null);
                views_checks.add(new WeakReference<CheckBox>(check_view.findViewById(R.id.checkBox3)));
                check_view.findViewById(R.id.textView14).setOnClickListener(view -> {
                    Intent intent= new Intent(TripDetailActivity.this, CheckOutActivity.class);
                    intent.putExtra("ID",getIntent().getStringExtra("ID"));
                    intent.putExtra("date",getIntent().getStringExtra("date"));
                    intent.putExtra("start",getIntent().getStringExtra("start"));
                    intent.putExtra("air",getIntent().getStringExtra("air"));
                    startActivity(intent);
                });
                e.onNext(new RowPosition(3, check_view));
                //divider
                frameLayout= new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(1)));
                frameLayout.setBackgroundColor(Color.parseColor("#c4c4c4"));
                e.onNext(new RowPosition(3, frameLayout));
            });
            e.onComplete();
        }, BackpressureStrategy.BUFFER).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    RowPosition row = (RowPosition) o;
                    switch (row.getPosition()) {
                        case 0:
                            legContainer.addView(row.getContent());
                            break;
                        case 1:
                            plane1Container.addView(row.getContent());
                            break;
                        case 2:
                            plane2Container.addView(row.getContent());
                            break;
                        case 3:
                            checkContainer.addView(row.getContent());
                            break;
                    }
                },
                        throwable ->
                                throwable.printStackTrace(),
                        () -> hideLoading());
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
//        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @OnClick(R.id.check_all)
    public void onViewClicked() {
        Stream.of(views_checks).forEach(checkBoxWeakReference -> {
            checkBoxWeakReference.get().setChecked(!checkBoxWeakReference.get().isChecked());
        });
    }
}
