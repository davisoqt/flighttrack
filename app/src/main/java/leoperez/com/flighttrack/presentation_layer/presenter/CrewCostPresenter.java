package leoperez.com.flighttrack.presentation_layer.presenter;

import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.data_layer.repository.TripListRepository;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.converter.CrewCostItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Crew;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.CrewCostView;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public class CrewCostPresenter extends TiPresenter<CrewCostView> {

    @Inject
    TripsDetailRepository repository;
    ArrayList<CrewCostItem> items;

    @Override
    public void attachView(@NonNull CrewCostView view) {
        super.attachView(view);
        requestIds();
    }

    private void requestIds() {
        repository.getCrewCostItems()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> items=o,throwable -> {throwable.printStackTrace();getView().showError("An error ocurred while load crew detail.");},
                        () -> getView().initList(items));
    }

    private void initList() {
    }

    @Inject
    public CrewCostPresenter() {
    }


}
