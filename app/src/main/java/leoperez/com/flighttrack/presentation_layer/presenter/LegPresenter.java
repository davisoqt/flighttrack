package leoperez.com.flighttrack.presentation_layer.presenter;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;

import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.converter.ConverterModel;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.LegView;

/**
 * Created by Leo Perez Ortíz on 9/11/17.
 */

public class LegPresenter extends TiPresenter<LegView> {

    @Inject
    TripsDetailRepository repository;

    @Inject
    public LegPresenter() {
    }

    @Override
    protected void onAttachView(@NonNull LegView view) {
        super.onAttachView(view);
        init();
    }

    private void init() {
        repository.getLegs()
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().setLegs((ArrayList<Leg>) o);
                    initLegs((ArrayList<Leg>) o);
                }, throwable -> {
                    ((Throwable) throwable).printStackTrace();
                });
    }

    private void initLegs(ArrayList<Leg> legs) {
        Observable.create(e -> {
            ArrayList<LegSpinnerItem> data = new ArrayList<>();
            Stream.of(legs).forEach(leg ->
                    data.add(ConverterModel.getLegsSpinner(leg)));
            e.onNext(data);
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().setLegItemData((ArrayList<LegSpinnerItem>) o);
                    getView().isOld(repository.isOld());
                    getView().loadDinamic();
                });
    }
}
