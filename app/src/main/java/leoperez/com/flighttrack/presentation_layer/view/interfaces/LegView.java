package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;

/**
 * Created by Leo Perez Ortíz on 9/11/17.
 */

public interface LegView extends BaseView {
    void setLegs(ArrayList<Leg> o);

    void setLegItemData(ArrayList<LegSpinnerItem> o);

    void loadDinamic();

    void isOld(boolean old);
}
