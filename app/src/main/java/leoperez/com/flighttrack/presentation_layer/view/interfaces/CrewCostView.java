package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import net.grandcentrix.thirtyinch.TiView;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.converter.CrewCostItem;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public interface CrewCostView extends BaseView {

    void initList(ArrayList<CrewCostItem> items);
}
