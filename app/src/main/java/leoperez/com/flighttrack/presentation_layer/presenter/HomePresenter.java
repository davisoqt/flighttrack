package leoperez.com.flighttrack.presentation_layer.presenter;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.app.FlightApi;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.data_layer.repository.MasterListRepository;
import leoperez.com.flighttrack.data_layer.repository.TripListRepository;
import leoperez.com.flighttrack.domain_layer.payload.home.masterList.FtMasterListPayLoad;
import leoperez.com.flighttrack.domain_layer.payload.home.trip.PayLoadTripList;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.TripDetail;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.home.HomeView;
import retrofit2.Retrofit;

/**
 * Created by Leo Perez Ortíz on 31/10/17.
 */

public class HomePresenter extends TiPresenter<HomeView>{

    private static final String TAG = "HomePresenter";
    @Inject
    FlightApi retrofit;
    @Inject
    MasterListRepository masterListRepository;
    @Inject
    TripListRepository tripListRepository;
    @Inject
    SharedPreferences preferences;
    private boolean dataok=false;

    @Inject
    public HomePresenter() {

    }

    @Override
    protected void onAttachView(@NonNull HomeView view) {
        super.onAttachView(view);
        view.showLOading("Loading...");
        loadHome();
    }

    public void loadHome() {
        //Test
        if(dataok) {
            loadLocal();
            return;
        }
        FtMasterListPayLoad payLoad = new FtMasterListPayLoad();
        payLoad.setDataJSON(new leoperez.com.flighttrack.domain_layer.payload.home.masterList.DataJSON());
        payLoad.getDataJSON().setSessionKey(preferences.getString(Constants.SESSION_KEY,""));
        PayLoadTripList payLoadTripList = new PayLoadTripList();
        payLoadTripList.setDataJSON(new leoperez.com.flighttrack.domain_layer.payload.home.trip.DataJSON());
        payLoadTripList.getDataJSON().setSessionKey(preferences.getString(Constants.SESSION_KEY,""));
        Observable.zip(retrofit.getMasterList(payLoad), retrofit.getTripList(payLoadTripList), (ftMasterListResponse, ftTripListResponse) -> {
            if (ftMasterListResponse.getReturnData() == null || ftMasterListResponse.getReturnData().size() == 0
                    || ftTripListResponse.getReturnData() == null || ftTripListResponse.getReturnData().size() == 0)
                throw new Exception("Error");
            masterListRepository.saveFTMasterlistSync(ftMasterListResponse.getReturnData().get(0).getResult().getUpdateMasterList());
            tripListRepository.insertTripListSync(ftTripListResponse.getReturnData().get(0).getResult().getTrips());
            dataok=true;
            return "ok";
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stringObservable -> {
                    if (stringObservable.equals("ok")) {
                        loadLocal();
                    }
                    else
                        getView().showErrorSection();
                    if(Constants.DEBUG)
                    loadLocal();
                    getView().hideDialog();
                }, throwable -> {
                    dataok=false;
                    throwable.printStackTrace();
                    getView().emptyContent();
                    loadLocal();
                });
    }

    private void loadLocal() {
        FlightApplication.credit_cards=masterListRepository.getCreditCards();
        tripListRepository.getTripList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if(o instanceof String)
                        Log.d(TAG, "loadLocal: ERROR");
                    else {
                        if(o==null || ((ArrayList<TripDetail>) o).size()==0)
                            getView().emptyContent();
                        else
                        getView().loadList((ArrayList<TripDetail>) o);
                    }
                },throwable -> throwable.printStackTrace());
    }
}
