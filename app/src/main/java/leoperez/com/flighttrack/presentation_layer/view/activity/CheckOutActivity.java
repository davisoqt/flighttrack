package leoperez.com.flighttrack.presentation_layer.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.data_layer.repository.MasterListRepository;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.presentation_layer.view.fragment.ACostFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewCostDetailFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewCostFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.CrewFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.LegFragment;
import leoperez.com.flighttrack.presentation_layer.view.fragment.PaxFragment;

public class CheckOutActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.trip)
    TextView trip;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.craft_id)
    TextView craftId;
    @BindView(R.id.update)
    TextView update;
    Fragment current;
    ArrayList<Leg> legs;
    @Inject
    TripsDetailRepository repository;
    @Inject
    MasterListRepository masterListRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ((FlightApplication) getApplication()).getAppComponent().inject(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ((NavigationView) findViewById(R.id.nav_view)).setNavigationItemSelectedListener(this);
        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> {
            if (drawer.isDrawerOpen(GravityCompat.START))
                drawer.closeDrawer(GravityCompat.START);
            else
                drawer.openDrawer(GravityCompat.START);
        });
        trip.setText("Trip ID: " + getIntent().getStringExtra("ID"));
        date.setText("Start date: " + getIntent().getStringExtra("start"));
        craftId.setText("AirCraft ID: " + getIntent().getStringExtra("air"));
        update.setText("Last Update: " + getIntent().getStringExtra("date"));
        gotoSection(LegFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.checkout) {
            // Handle the camera action
            finish();
        } else if (id == R.id.leg) {
            gotoSection(LegFragment.newInstance());
        } else if (id == R.id.pax) {
            gotoSection(PaxFragment.newInstance());
        } else if (id == R.id.crew) {
            gotoSection(CrewFragment.newInstance(masterListRepository.getCrewIds()));
        } else if (id == R.id.cost) {
            gotoSection(ACostFragment.newInstance());
        } else if (id == R.id.crew_cost) {
            gotoSection(CrewCostFragment.newInstance());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void gotoSection(Fragment crewFragment) {
        current = crewFragment;
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward)
                .replace(R.id.section, crewFragment).commitAllowingStateLoss();
    }

    public void showCrewCostDetail(String id) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward)
                .replace(R.id.crew_detail, CrewCostDetailFragment.newInstance(id)).addToBackStack("d").commitAllowingStateLoss();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
//        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @OnClick(R.id.out)
    public void onViewClicked() {
        Intent i = new Intent(this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
