package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import net.grandcentrix.thirtyinch.TiView;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public interface CrewCostDetailView extends BaseView {

    void initSpinner(CrewCostsList item, ArrayList<String> cards, ArrayList<String> crew_id, ArrayList<String> icao,
                     String trip_id,String aircraft,String date,String update);

    void initFields(CrewCostsList crew);

    void isOld(boolean old);
}
