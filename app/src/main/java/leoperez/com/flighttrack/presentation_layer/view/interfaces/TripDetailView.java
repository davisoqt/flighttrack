package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import android.content.Context;

import net.grandcentrix.thirtyinch.TiView;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

public interface TripDetailView extends BaseView {

    void load();
    void showLegs(ArrayList<LegItem> legs);
}
