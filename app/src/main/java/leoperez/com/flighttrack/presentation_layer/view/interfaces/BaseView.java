package leoperez.com.flighttrack.presentation_layer.view.interfaces;

import net.grandcentrix.thirtyinch.TiView;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

public interface BaseView extends TiView {

    void shoLoadng();
    void hideLoading();
    void initLoading();

    void showError(String error_trip_detail);
}
