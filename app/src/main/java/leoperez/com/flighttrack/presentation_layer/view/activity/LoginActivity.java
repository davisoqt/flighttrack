package leoperez.com.flighttrack.presentation_layer.view.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.app.FlightApi;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.payload.DataJSON;
import leoperez.com.flighttrack.domain_layer.payload.PayLoadLogin;
import leoperez.com.flighttrack.domain_layer.payload.home.masterList.FtMasterListPayLoad;
import leoperez.com.flighttrack.domain_layer.payload.home.trip.PayLoadTripList;
import leoperez.com.flighttrack.data_layer.repository.MasterListRepository;
import leoperez.com.flighttrack.data_layer.repository.TripListRepository;

public class LoginActivity extends BaseActivity {

    @Inject
    FlightApi retrofit;
    @Inject
    SharedPreferences preferences;
    @Inject
    MasterListRepository resMasterListRepository;
    @Inject
    TripListRepository tripListRepository;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.pass)
    EditText pass;
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.checkBox)
    CheckBox checkBox;
    private MaterialDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        iniDagger();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if(preferences.getBoolean(Constants.SAVE_CREDENTIAL,false))
            checkBox.setChecked(true);
    }

    @Override
    public void iniDagger() {
        ((FlightApplication) getApplication()).getAppComponent().inject(this);
    }

    @OnClick(R.id.imageView7)
    public void onViewClicked() {
        if(validate()) {
            showLoading();
            //create payload
            PayLoadLogin payLoadLogin = new PayLoadLogin();
            payLoadLogin.setmDataJSON(new DataJSON());
            payLoadLogin.getDataJSON().setmCustomCode(code.getText().toString());
            payLoadLogin.getDataJSON().setmPassword(pass.getText().toString());
            payLoadLogin.getDataJSON().setmLogin(name.getText().toString());
            if (Constants.DEBUG) {
                Log.d("PayLoadLogin", new Gson().toJson(payLoadLogin));
            }
            retrofit.login(payLoadLogin)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(loginResponse -> {
                        //save session key
                        if (loginResponse.getReturnData() != null && loginResponse.getReturnData().size() > 0) {
                            preferences.edit().putString(Constants.SESSION_KEY, loginResponse.getReturnData().get(0).getSessionKey())
                                    .putString(Constants.NAME, name.getText().toString())
                                    .putString(Constants.CUSTOM_CODE, code.getText().toString()).commit();
                            if (checkBox.isChecked())
                                preferences.edit().putBoolean(Constants.SAVE_CREDENTIAL, true).commit();
                            else
                                preferences.edit().putBoolean(Constants.SAVE_CREDENTIAL, false).commit();
                        } else
                            showError(getString(R.string.login_error));
                        //loadHome
                        Constants.launchActivityFinish(LoginActivity.this, HomeActivity.class);
                        mDialog.dismiss();
                    }, throwable -> {
                        showError(getString(R.string.login_error));
                    });
        }
        else {
            Toast.makeText(this,"Invalid credential data!",Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validate() {
        return !TextUtils.isEmpty(name.getText().toString()) && !TextUtils.isEmpty(pass.getText().toString()) && !TextUtils.isEmpty(code.getText().toString());
    }

    //loading Dialog
    private void showLoading() {
        mDialog = new MaterialDialog.Builder(this)
                .progress(true, 30)
                .content(R.string.login)
                .show();
    }

    public void showError(String msg) {
        mDialog.dismiss();
        new MaterialDialog.Builder(this)
                .title("Error")
                .content(R.string.login_error)
                .positiveText("Ok")
                .show();
    }
}
