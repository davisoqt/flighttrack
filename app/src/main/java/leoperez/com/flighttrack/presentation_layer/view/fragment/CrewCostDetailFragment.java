package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import net.grandcentrix.thirtyinch.TiFragment;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindBool;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.RowPosition;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewCostFieldList;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList;
import leoperez.com.flighttrack.presentation_layer.presenter.CrewCostDetailPresenter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.spinner.SpinnerCrewAdapter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.CrewCostDetailView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrewCostDetailFragment extends TiFragment<CrewCostDetailPresenter, CrewCostDetailView> implements CrewCostDetailView {


    private static final String TAG = CrewCostDetailFragment.class.getCanonicalName();
    @BindView(R.id.spinner_crew)
    Spinner spinnerCrew;
    @BindView(R.id.et_date)
    EditText etDate;
    @BindView(R.id.spinner_card)
    Spinner spinnerCard;
    @BindView(R.id.spinner_icao)
    Spinner spinnerIcao;
    @BindView(R.id.container)
    LinearLayout container;
    Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.trip)
    TextView trip;
    @BindView(R.id.date)
    TextView time;
    @BindView(R.id.craft_id)
    TextView craftId;
    @BindView(R.id.update)
    TextView update;
    @BindBool(R.bool.is_tablet)
    boolean isTablet;
    private MaterialDialog materialDialog;
    @Inject
    CrewCostDetailPresenter presenter;
    String crew_id;

    //Adapter
    SpinnerCrewAdapter adapter_card;
    SpinnerCrewAdapter adapter_crew;
    SpinnerCrewAdapter adapter_icao;

    private LayoutInflater inflater;
    private View selected_view;
    private boolean isOld;
    private LinearLayout column1;
    private LinearLayout column2;

    public CrewCostDetailFragment() {
        // Required empty public constructor
    }

    public static CrewCostDetailFragment newInstance(String crew_id) {
        Bundle args = new Bundle();
        CrewCostDetailFragment fragment = new CrewCostDetailFragment();
        args.putString("id", crew_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((FlightApplication) getActivity().getApplication()).getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        crew_id = getArguments().getString("id");
        presenter.setCrewId(crew_id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        this.inflater=inflater;
        View view = inflater.inflate(R.layout.fragment_crew_cost_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        initLoading();
        toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(),R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(view1 -> getActivity().onBackPressed());
        if(isTablet) {
            column1 = view.findViewById(R.id.c1);
            column2 = view.findViewById(R.id.c2);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth() * 0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            RelativeLayout.LayoutParams lpp = new RelativeLayout.LayoutParams((int) (ScreenUtils.getScreenWidth() * 0.45), ViewGroup.LayoutParams.WRAP_CONTENT);
            lpp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            view.findViewById(R.id.c2_container).setLayoutParams(lp);
            view.findViewById(R.id.c1_container).setLayoutParams(lpp);
        }
        return view;
    }

    @OnClick(R.id.et_date)
    public void onViewClicked() {
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener((dialog, year, monthOfYear, dayOfMonth) -> {
                    String y = (year + "").substring(2);
                    etDate.setText(monthOfYear + "/" + dayOfMonth + "/" + y);
                })
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setDoneText("Ok")
                .setCancelText("Cancel");
        cdp.show(getChildFragmentManager(), "s");
    }

    @Override
    public void shoLoadng() {
        materialDialog.show();
    }

    @Override
    public void hideLoading() {
        materialDialog.dismiss();
    }

    @Override
    public void initLoading() {
        materialDialog = new MaterialDialog.Builder(getContext())
                .content("Loading")
                .progress(true, 30)
                .build();
    }

    @Override
    public void showError(String error_trip_detail) {

    }

    @NonNull
    @Override
    public CrewCostDetailPresenter providePresenter() {
        return presenter;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void initSpinner(CrewCostsList item, ArrayList<String> cards, ArrayList<String> crew_id, ArrayList<String> icao,
                            String trip_id,String aircraft,String date,String update) {
        if(isOld){
            etDate.setEnabled(false);
            spinnerCrew.setEnabled(false);
            spinnerIcao.setEnabled(false);
            spinnerCard.setEnabled(false);
        }
        adapter_card=new SpinnerCrewAdapter(cards, getContext());
        spinnerCard.setAdapter(adapter_card);
        spinnerCard.setSelection(adapter_card.getItemPosition(item.getCCard()));
        adapter_crew=new SpinnerCrewAdapter(crew_id, getContext());
        spinnerCrew.setAdapter(adapter_crew);
        spinnerCrew.setSelection(adapter_crew.getItemPosition(item.getCrewID()));
        adapter_icao= new SpinnerCrewAdapter(icao, getContext());
        spinnerIcao.setAdapter(adapter_icao);
        spinnerIcao.setSelection(adapter_icao.getItemPosition(item.getICAO()));
        trip.setText("ID: " +trip_id);
        craftId.setText("AirCraft ID: "+aircraft);
        time.setText("Start date: "+date);
        this.update.setText("Last Update: "+update);
        if(!TextUtils.isEmpty(item.getCostDate()))
            etDate.setText(item.getCostDate());
        initFields(item);
        initListeners();
    }

    private void initListeners() {
        spinnerCrew.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                presenter.reloadWithCrewId((String) adapter_crew.getItem(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void initFields(CrewCostsList crew) {
        if(!isTablet) {
            container.removeAllViews();
            Flowable.create(e -> {
                        for (CrewCostFieldList field : crew.getCrewCostFieldList()
                                ) {
                            View item_view = inflater.inflate(R.layout.crew_cost_item, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.editText);
                            edit.setHint("Enter " + field.getFieldLabel());
                            if (!TextUtils.isEmpty(field.getFieldValue()))
                                edit.setText(field.getFieldValue());
                            //InputType
                            Log.d(TAG, "init: fielType " + field.getFieldType());
                            if (field.getFieldType().equals("A"))
                                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                            else if (field.getFieldType().equals("I") || field.getFieldType().equals("R"))
                                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                            else if (field.getFieldType().equals("T")) {
                                edit.setFocusable(false);
                                edit.setFocusableInTouchMode(false);
                                edit.setOnClickListener(view -> {
                                    selected_view = view;
                                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                                            .setStartTime(10, 10)
                                            .setOnTimeSetListener((dialog, hourOfDay, minute) -> {
                                                ((EditText) selected_view).setText(hourOfDay + ":" + minute);
                                            })
                                            .setDoneText("Ok")
                                            .setCancelText("Cancel");
                                    rtpd.show(getChildFragmentManager(), "d");
                                });
                            } else if (field.getFieldType().equals("D")) {
                                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                                        .setOnDateSetListener((dialog, year, monthOfYear, dayOfMonth) -> {
                                            String y = (year + "").substring(2);
                                            ((EditText) selected_view).setText(monthOfYear + "/" + dayOfMonth + "/" + y);
                                        })
                                        .setFirstDayOfWeek(Calendar.SUNDAY)
                                        .setDoneText("Ok")
                                        .setCancelText("Cancel");
                                cdp.show(getChildFragmentManager(), "s");
                            }
                            if (TextUtils.isEmpty(field.getNotes()))
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
                            else
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));

                            if (!isOld) {
                                //click label
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    if (field.getFieldType().equals("T") || field.getFieldType().equals("D"))
                                        ((EditText) view.getTag()).performClick();
                                    else
                                        KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            } else {
                                edit.setEnabled(false);
                            }
                            e.onNext(item_view);
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        container.addView((View) item_view);
                    }, throwable -> throwable.printStackTrace(), () -> {
                        materialDialog.dismiss();
                    });
        }else {
            final int[] position = {0};
            column1.removeAllViews();
            column2.removeAllViews();
            Flowable.create(e -> {
                        for (CrewCostFieldList field : crew.getCrewCostFieldList()
                                ) {
                            View item_view = inflater.inflate(R.layout.crew_cost_item, null);
                            //label
                            ((TextView) item_view.findViewById(R.id.textView11)).setText(field.getFieldLabel());
                            EditText edit = (EditText) item_view.findViewById(R.id.editText);
                            edit.setHint("ENTER DATA");
                            if (!TextUtils.isEmpty(field.getFieldValue()))
                                edit.setText(field.getFieldValue());
                            //InputType
                            Log.d(TAG, "init: fielType " + field.getFieldType());
                            if (field.getFieldType().equals("A"))
                                edit.setInputType(InputType.TYPE_CLASS_TEXT);
                            else if (field.getFieldType().equals("I") || field.getFieldType().equals("R"))
                                edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                            else if (field.getFieldType().equals("T")) {
                                edit.setFocusable(false);
                                edit.setFocusableInTouchMode(false);
                                edit.setOnClickListener(view -> {
                                    selected_view = view;
                                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                                            .setStartTime(10, 10)
                                            .setOnTimeSetListener((dialog, hourOfDay, minute) -> {
                                                ((EditText) selected_view).setText(hourOfDay + ":" + minute);
                                            })
                                            .setDoneText("Ok")
                                            .setCancelText("Cancel");
                                    rtpd.show(getChildFragmentManager(), "d");
                                });
                            } else if (field.getFieldType().equals("D")) {
                                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                                        .setOnDateSetListener((dialog, year, monthOfYear, dayOfMonth) -> {
                                            String y = (year + "").substring(2);
                                            ((EditText) selected_view).setText(monthOfYear + "/" + dayOfMonth + "/" + y);
                                        })
                                        .setFirstDayOfWeek(Calendar.SUNDAY)
                                        .setDoneText("Ok")
                                        .setCancelText("Cancel");
                                cdp.show(getChildFragmentManager(), "s");
                            }
                            if (TextUtils.isEmpty(field.getNotes()))
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.white));
                            else
                                ((ImageView) item_view.findViewById(R.id.ic_note)).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.green));

                            if (!isOld) {
                                //click label
                                item_view.findViewById(R.id.textView11).setTag(edit);
                                item_view.findViewById(R.id.textView11).setOnClickListener(view -> {
                                    if (field.getFieldType().equals("T") || field.getFieldType().equals("D"))
                                        ((EditText) view.getTag()).performClick();
                                    else
                                        KeyboardUtils.showSoftInput(((EditText) view.getTag()));
//                            ((EditText) view.getTag()).requestFocus();
                                });
                            } else {
                                edit.setEnabled(false);
                            }
                            e.onNext(new RowPosition(position[0]++,item_view));
                            if(position[0]==2)
                                position[0]=0;
                        }
                        e.onComplete();
                    }
                    , BackpressureStrategy.BUFFER).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item_view -> {
                        switch (((RowPosition)item_view).getPosition()){
                            case 0:
                                column1.addView(((RowPosition)item_view).getContent());
                                break;
                            case 1:
                                column2.addView(((RowPosition)item_view).getContent());
                                break;
                        }
                    }, throwable -> throwable.printStackTrace(), () -> {
                        materialDialog.dismiss();
                    });
        }
    }

    @Override
    public void isOld(boolean old) {
        this.isOld=old;
    }
}
