package leoperez.com.flighttrack.presentation_layer.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import net.grandcentrix.thirtyinch.TiActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.Constants;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.app.Utils;
import leoperez.com.flighttrack.domain_layer.response.home.triplist.TripDetail;
import leoperez.com.flighttrack.presentation_layer.presenter.HomePresenter;
import leoperez.com.flighttrack.presentation_layer.view.adapter.home.TrpisAdapter;
import leoperez.com.flighttrack.presentation_layer.view.fragment.TripDetailFragment;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.home.HomeView;
import retrofit2.Retrofit;

public class HomeActivity extends TiActivity<HomePresenter,HomeView>
        implements HomeView,View.OnClickListener{

//    @Inject
//    OkHttpClient mOkHttpClient;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Retrofit retrofit;
    @Inject
    HomePresenter presenter;
    @BindView(R.id.rv)
    RecyclerView rv;
    private MaterialDialog mDialog;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initDagger();
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        iniViews();
    }

    private void iniViews() {
        mDialog= new MaterialDialog.Builder(this)
                .progress(true,30)
                .cancelable(false)
                .build();
        ((FlightApplication) getApplication()).getAppComponent().inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> {
            if(drawer.isDrawerOpen(GravityCompat.START))
                drawer.closeDrawer(GravityCompat.START);
            else
                drawer.openDrawer(GravityCompat.START);
        });
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    }

    private void initDagger() {
        ((FlightApplication)getApplication()).getAppComponent().inject(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
//        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void showLOading(String text) {
        mDialog.setContent(text);
        mDialog.show();
    }

    @Override
    public void loadList(ArrayList<TripDetail> trips) {
        rv.setAdapter(new TrpisAdapter(this,trips));
        mDialog.dismiss();
    }

    @Override
    public void hideDialog() {
        mDialog.dismiss();
    }

    @NonNull
    @Override
    public HomePresenter providePresenter() {
        return presenter;
    }

//    public void gotoSection(Fragment fragment) {
//        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward).replace(R.id.menu_container,fragment).commitAllowingStateLoss();
//    }


    @Override
    public void showErrorSection() {
        new MaterialDialog.Builder(this)
                .title("Error")
                .content("There was an error triying to load tip list.")
                .positiveText("Cancel")
                .negativeText("Retry")
                .onNegative((dialog, which) -> presenter.loadHome())
                .show();
    }

    @Override
    public void emptyContent() {
        new MaterialDialog.Builder(this)
                .content("No trips found.")
                .contentGravity(GravityEnum.CENTER)
                .neutralText("Ok")
                .buttonsGravity(GravityEnum.CENTER)
                .onNeutral((dialog, which) -> {
                    Constants.launchActivityFinish(this,LoginActivity.class);
                }).show();
    }

    @Override
    public void onClick(View view) {

    }
    @OnClick(R.id.out)
    public void onViewClicked() {
        Intent i = new Intent(this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
