package leoperez.com.flighttrack.presentation_layer.view.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import net.grandcentrix.thirtyinch.TiFragment;
import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.domain_layer.converter.LegItem;
import leoperez.com.flighttrack.presentation_layer.presenter.TripDetailPresenter;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.TripDetailView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TripDetailFragment extends TiFragment implements TripDetailView {


    @BindView(R.id.leg_container)
    LinearLayout legContainer;
    @BindView(R.id.plane1_container)
    LinearLayout plane1Container;
    @BindView(R.id.plane2_container)
    LinearLayout plane2Container;
    @BindView(R.id.check_container)
    LinearLayout checkContainer;
    Unbinder unbinder;
    @BindView(R.id.trip)
    TextView trip;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.craft_id)
    TextView craftId;
    @BindView(R.id.update)
    TextView update;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private LayoutInflater inflater;
    private MaterialDialog mDialog;
    @Inject
    TripDetailPresenter presenter;
    String tripID;
    private String dateS;
    private String start;
    private String air;

    public TripDetailFragment() {
        // Required empty public constructor
    }

    public static TripDetailFragment newInstance(String tripID, String date, String air, String start) {
        Bundle args = new Bundle();
        args.putString("ID", tripID);
        args.putString("date", date);
        args.putString("start", start);
        args.putString("air", air);
        TripDetailFragment fragment = new TripDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tripID = getArguments().getString("ID");
        dateS = getArguments().getString("date");
        start = getArguments().getString("start");
        air = getArguments().getString("air");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_trip_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(view1 -> {
            Toast.makeText(getContext(),"ljsfhksjf",Toast.LENGTH_SHORT).show();
        });
        initLoading();
        presenter.setTripID(tripID);
        presenter.requestData();
        trip.setText("ID: " + tripID);
        date.setText("Start date: " + start);
        craftId.setText("AirCraft ID: " + air);
        update.setText("Last Update: " + dateS);
        return view;
    }

    @NonNull
    @Override
    public TiPresenter providePresenter() {
        return presenter;
    }

    @Override
    public void onAttach(Context context) {
        ((FlightApplication) getActivity().getApplication()).getAppComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void shoLoadng() {
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
    }

    @Override
    public void initLoading() {
        mDialog = new MaterialDialog.Builder(getContext()).progress(true, 30).build();
    }

    @Override
    public void showError(String error_trip_detail) {
        new MaterialDialog.Builder(getContext())
                .title("Error")
                .content(error_trip_detail)
                .positiveText("Ok")
                .show();
    }

    @Override
    public void load() {
        shoLoadng();
    }

    @Override
    public void showLegs(ArrayList<LegItem> legs) {
        hideLoading();
        for (int i = 0; i < legs.size(); i++) {
            View leg_view = inflater.inflate(R.layout.trip_detail_leg, legContainer, true);
            ((TextView) leg_view.findViewById(R.id.textView9)).setText(legs.get(i).getID());
            //change leg ID
            View depart_view = inflater.inflate(R.layout.trip_depart, plane1Container, true);
            ((TextView) depart_view.findViewById(R.id.textView10)).setText(legs.get(i).getDepart());
            //change depart
            View arriw_view = inflater.inflate(R.layout.trip_arrive, plane2Container, true);
            ((TextView) arriw_view.findViewById(R.id.textView10)).setText(legs.get(i).getDepart());
            //change arrive
            View check_view = inflater.inflate(R.layout.trip_check_out, checkContainer, true);
            ((TextView) arriw_view.findViewById(R.id.textView10)).setText(legs.get(i).getDepart());

        }
    }
}
