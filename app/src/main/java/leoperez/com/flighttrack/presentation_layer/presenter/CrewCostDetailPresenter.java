package leoperez.com.flighttrack.presentation_layer.presenter;

import android.support.annotation.NonNull;

import net.grandcentrix.thirtyinch.TiPresenter;

import java.util.ArrayList;
import java.util.Observable;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import leoperez.com.flighttrack.app.FlightApplication;
import leoperez.com.flighttrack.data_layer.repository.MasterListRepository;
import leoperez.com.flighttrack.data_layer.repository.TripsDetailRepository;
import leoperez.com.flighttrack.domain_layer.converter.TripBasicData;
import leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList;
import leoperez.com.flighttrack.presentation_layer.view.interfaces.CrewCostDetailView;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public class CrewCostDetailPresenter extends TiPresenter<CrewCostDetailView> {

    private String crewId;
    @Inject
    TripsDetailRepository repository;
    @Inject
    MasterListRepository masterListRepository;
    ArrayList<String> crew_ids;
    ArrayList<String> icaos;
    CrewCostsList crewCostsList;
    TripBasicData basicData;

    @Inject
    public CrewCostDetailPresenter() {
    }

    @Override
    public void attachView(@NonNull CrewCostDetailView view) {
        super.attachView(view);
        requestData();
    }

    public void requestData() {
        getView().initLoading();
        io.reactivex.Observable.create(e -> {
            crew_ids = repository.getCrewCostIds();
            icaos = repository.getICAO();
            crewCostsList = repository.getCrewCost(crewId);
            basicData = repository.getBasicData();
            e.onNext("");
            e.onComplete();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().isOld(repository.isOld());
                            getView().initSpinner(crewCostsList, FlightApplication.credit_cards, crew_ids, icaos,
                                    basicData.getTrip_id(), basicData.getAir_craft(), basicData.getDate(), basicData.getUpdate());
                        },
                        throwable -> throwable.printStackTrace());
    }

    public void reloadWithCrewId(String id){
        io.reactivex.Observable.create(e -> {
            crewCostsList=repository.getCrewCost(id);
            e.onNext("");
            e.onComplete();
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {getView().initFields(crewCostsList);},
                        throwable -> throwable.printStackTrace());
    }

    public void setCrewId(String crewId) {
        this.crewId = crewId;
    }

    public String getCrewId() {
        return crewId;
    }
}
