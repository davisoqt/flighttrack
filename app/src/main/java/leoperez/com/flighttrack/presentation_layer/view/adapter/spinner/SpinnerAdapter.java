package leoperez.com.flighttrack.presentation_layer.view.adapter.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import leoperez.com.flighttrack.R;
import leoperez.com.flighttrack.domain_layer.converter.LegSpinnerItem;

/**
 * Created by Leo Perez Ortíz on 5/11/17.
 */

public class SpinnerAdapter extends BaseAdapter {

    ArrayList<LegSpinnerItem> items;
    Context context;

    public SpinnerAdapter(ArrayList<LegSpinnerItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View converter= view;
        if(view== null){
            converter= LayoutInflater.from(context).inflate(R.layout.spinner_item,null);
        }
        ((TextView)converter.findViewById(R.id.textView12)).setText("Leg "+items.get(i).getText());
        converter.setTag(i);
        return converter;
    }
}
