
package leoperez.com.flighttrack.domain_layer.response.trip;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Result {

    @SerializedName("AircraftID")
    private String mAircraftID;
    @SerializedName("CrewCostsList")
    private List<leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList> mCrewCostsList;
    @SerializedName("LastUpdated")
    private String mLastUpdated;
    @SerializedName("Legs")
    private List<Leg> mLegs;
    @SerializedName("StartDate")
    private String mStartDate;
    @SerializedName("TripICAOList")
    private ArrayList<String> mTripICAOList;
    @SerializedName("TripID")
    private String mTripID;
    @SerializedName("TripPDFList")
    private List<Object> mTripPDFList;

    public String getAircraftID() {
        return mAircraftID;
    }

    public void setAircraftID(String AircraftID) {
        mAircraftID = AircraftID;
    }

    public List<leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList> getCrewCostsList() {
        return mCrewCostsList;
    }

    public void setCrewCostsList(List<leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList> CrewCostsList) {
        mCrewCostsList = CrewCostsList;
    }

    public String getLastUpdated() {
        return mLastUpdated;
    }

    public void setLastUpdated(String LastUpdated) {
        mLastUpdated = LastUpdated;
    }

    public List<Leg> getLegs() {
        return mLegs;
    }

    public void setLegs(List<Leg> Legs) {
        mLegs = Legs;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String StartDate) {
        mStartDate = StartDate;
    }

    public ArrayList<String> getTripICAOList() {
        return mTripICAOList;
    }

    public void setTripICAOList(ArrayList<String> TripICAOList) {
        mTripICAOList = TripICAOList;
    }

    public String getTripID() {
        return mTripID;
    }

    public void setTripID(String TripID) {
        mTripID = TripID;
    }

    public List<Object> getTripPDFList() {
        return mTripPDFList;
    }

    public void setTripPDFList(List<Object> TripPDFList) {
        mTripPDFList = TripPDFList;
    }

}
