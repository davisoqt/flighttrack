
package leoperez.com.flighttrack.domain_layer.response.save_crew;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SaveCrewResponse {

    @SerializedName("error")
    private String Error;
    @SerializedName("failure")
    private String Failure;
    @SerializedName("failureMessage")
    private String FailureMessage;
    @SerializedName("inputProcessId")
    private String InputProcessId;
    @SerializedName("message")
    private String Message;
    @SerializedName("returnCode")
    private String ReturnCode;
    @SerializedName("returnData")
    private List<ReturnDatum> ReturnData;

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }

    public String getFailure() {
        return Failure;
    }

    public void setFailure(String failure) {
        Failure = failure;
    }

    public String getFailureMessage() {
        return FailureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        FailureMessage = failureMessage;
    }

    public String getInputProcessId() {
        return InputProcessId;
    }

    public void setInputProcessId(String inputProcessId) {
        InputProcessId = inputProcessId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public List<ReturnDatum> getReturnData() {
        return ReturnData;
    }

    public void setReturnData(List<ReturnDatum> returnData) {
        ReturnData = returnData;
    }

}
