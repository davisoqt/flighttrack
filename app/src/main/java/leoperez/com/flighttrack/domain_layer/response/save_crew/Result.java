
package leoperez.com.flighttrack.domain_layer.response.save_crew;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

import leoperez.com.flighttrack.domain_layer.payload.save_crew.Request;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Result {

    @Expose
    private Request Request;

    public Request getRequest() {
        return Request;
    }

    public void setRequest(Request Request) {
        this.Request = Request;
    }

}
