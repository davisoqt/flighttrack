
package leoperez.com.flighttrack.domain_layer.response.home.triplist;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class FtTripListResponse {

    @SerializedName("error")
    private String mError;
    @SerializedName("failure")
    private String mFailure;
    @SerializedName("failureMessage")
    private String mFailureMessage;
    @SerializedName("inputProcessId")
    private String mInputProcessId;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("returnCode")
    private String mReturnCode;
    @SerializedName("returnData")
    private List<ReturnDatum> mReturnData;

    public String getError() {
        return mError;
    }

    public void setError(String error) {
        mError = error;
    }

    public String getFailure() {
        return mFailure;
    }

    public void setFailure(String failure) {
        mFailure = failure;
    }

    public String getFailureMessage() {
        return mFailureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        mFailureMessage = failureMessage;
    }

    public String getInputProcessId() {
        return mInputProcessId;
    }

    public void setInputProcessId(String inputProcessId) {
        mInputProcessId = inputProcessId;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public void setReturnCode(String returnCode) {
        mReturnCode = returnCode;
    }

    public List<ReturnDatum> getReturnData() {
        return mReturnData;
    }

    public void setReturnData(List<ReturnDatum> returnData) {
        mReturnData = returnData;
    }

}
