package leoperez.com.flighttrack.domain_layer.converter;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

public class LegItem {
    private String ID;
    private String depart;
    private String arriv;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArriv() {
        return arriv;
    }

    public void setArriv(String arriv) {
        this.arriv = arriv;
    }

    public LegItem(String ID, String depart, String arriv) {
        this.ID = ID;
        this.depart = depart;
        this.arriv = arriv;
    }
}
