
package leoperez.com.flighttrack.domain_layer.response.trip;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PaxListTrip {

    @SerializedName("PaxId")
    private String mPaxId;
    @SerializedName("PaxPhone1")
    private String mPaxPhone1;
    @SerializedName("PaxPhone2")
    private String mPaxPhone2;

    public String getPaxId() {
        return mPaxId;
    }

    public void setPaxId(String PaxId) {
        mPaxId = PaxId;
    }

    public String getPaxPhone1() {
        return mPaxPhone1;
    }

    public void setPaxPhone1(String PaxPhone1) {
        mPaxPhone1 = PaxPhone1;
    }

    public String getPaxPhone2() {
        return mPaxPhone2;
    }

    public void setPaxPhone2(String PaxPhone2) {
        mPaxPhone2 = PaxPhone2;
    }

}
