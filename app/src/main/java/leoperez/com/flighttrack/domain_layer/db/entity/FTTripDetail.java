package leoperez.com.flighttrack.domain_layer.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

@Entity
public class FTTripDetail {
    private String json;
    @Id
    private long id;
    @Generated(hash = 1856736038)
    public FTTripDetail(String json, long id) {
        this.json = json;
        this.id = id;
    }

    @Generated(hash = 856840181)
    public FTTripDetail() {
    }

    public String getJson() {
        return this.json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
