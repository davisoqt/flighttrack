
package leoperez.com.flighttrack.domain_layer.payload.save_accost;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AcCostSave {

    @SerializedName("dataJSON")
    private leoperez.com.flighttrack.domain_layer.payload.save_accost.DataJSON DataJSON;
    @SerializedName("process_id")
    private String ProcessId="doFTPutACCost";

    public leoperez.com.flighttrack.domain_layer.payload.save_accost.DataJSON getDataJSON() {
        return DataJSON;
    }

    public void setDataJSON(leoperez.com.flighttrack.domain_layer.payload.save_accost.DataJSON dataJSON) {
        DataJSON = dataJSON;
    }

    public String getProcessId() {
        return ProcessId;
    }

    public void setProcessId(String processId) {
        ProcessId = processId;
    }

}
