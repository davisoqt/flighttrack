
package leoperez.com.flighttrack.domain_layer.payload.trip;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Request {

    @SerializedName("TripID")
    private String mTripID;

    public String getTripID() {
        return mTripID;
    }

    public void setTripID(String TripID) {
        mTripID = TripID;
    }

}
