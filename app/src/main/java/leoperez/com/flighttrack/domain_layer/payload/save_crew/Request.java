
package leoperez.com.flighttrack.domain_layer.payload.save_crew;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

import leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Request {

    @Expose
    private leoperez.com.flighttrack.domain_layer.payload.save_crew.Crew Crew;
    @Expose
    private List<leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList> CrewFieldList;
    @Expose
    private String LegNumber;
    @Expose
    private String TripID;

    public leoperez.com.flighttrack.domain_layer.payload.save_crew.Crew getCrew() {
        return Crew;
    }

    public void setCrew(leoperez.com.flighttrack.domain_layer.payload.save_crew.Crew Crew) {
        this.Crew = Crew;
    }

    public List<CrewFieldList> getCrewFieldList() {
        return CrewFieldList;
    }

    public void setCrewFieldList(List<CrewFieldList> CrewFieldList) {
        this.CrewFieldList = CrewFieldList;
    }

    public String getLegNumber() {
        return LegNumber;
    }

    public void setLegNumber(String LegNumber) {
        this.LegNumber = LegNumber;
    }

    public String getTripID() {
        return TripID;
    }

    public void setTripID(String TripID) {
        this.TripID = TripID;
    }

}
