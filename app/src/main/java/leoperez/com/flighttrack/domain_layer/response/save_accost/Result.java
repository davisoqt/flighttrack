
package leoperez.com.flighttrack.domain_layer.response.save_accost;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Result {

    @Expose
    private leoperez.com.flighttrack.domain_layer.response.save_accost.Request Request;

    public leoperez.com.flighttrack.domain_layer.response.save_accost.Request getRequest() {
        return Request;
    }

    public void setRequest(leoperez.com.flighttrack.domain_layer.response.save_accost.Request Request) {
        this.Request = Request;
    }

}
