
package leoperez.com.flighttrack.domain_layer.payload;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataJSON {

    @SerializedName("APIKey")
    private String mAPIKey="abcdefghijklmnop";
    @SerializedName("Action")
    private String mAction="Login";
    @SerializedName("CustomCode")
    private String mCustomCode;
    @SerializedName("LastUpdateDate")
    private String mLastUpdateDate;
    @SerializedName("Login")
    private String mLogin;
    @SerializedName("Password")
    private String mPassword;
    @SerializedName("SessionKey")
    private String mSessionKey;
    @SerializedName("UUID")
    private String mUUID="57DA814E-0419-4B37-971D-15E124358330";

    public String getAPIKey() {
        return mAPIKey;
    }

    public String getmAPIKey() {
        return mAPIKey;
    }

    public void setmAPIKey(String mAPIKey) {
        this.mAPIKey = mAPIKey;
    }

    public String getmAction() {
        return mAction;
    }

    public void setmAction(String mAction) {
        this.mAction = mAction;
    }

    public String getmCustomCode() {
        return mCustomCode;
    }

    public void setmCustomCode(String mCustomCode) {
        this.mCustomCode = mCustomCode;
    }

    public String getmLastUpdateDate() {
        return mLastUpdateDate;
    }

    public void setmLastUpdateDate(String mLastUpdateDate) {
        this.mLastUpdateDate = mLastUpdateDate;
    }

    public String getmLogin() {
        return mLogin;
    }

    public void setmLogin(String mLogin) {
        this.mLogin = mLogin;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmSessionKey() {
        return mSessionKey;
    }

    public void setmSessionKey(String mSessionKey) {
        this.mSessionKey = mSessionKey;
    }

    public String getmUUID() {
        return mUUID;
    }

    public void setmUUID(String mUUID) {
        this.mUUID = mUUID;
    }
}
