
package leoperez.com.flighttrack.domain_layer.response.home.triplist;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Result {

    @SerializedName("Trips")
    private leoperez.com.flighttrack.domain_layer.response.home.triplist.Trips mTrips;

    public leoperez.com.flighttrack.domain_layer.response.home.triplist.Trips getTrips() {
        return mTrips;
    }

    public void setTrips(leoperez.com.flighttrack.domain_layer.response.home.triplist.Trips Trips) {
        mTrips = Trips;
    }

}
