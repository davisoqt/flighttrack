
package leoperez.com.flighttrack.domain_layer.response.trip;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class LegFieldListTrip {

    @SerializedName("FieldLabel")
    private String mFieldLabel;
    @SerializedName("FieldNumber")
    private String mFieldNumber;
    @SerializedName("FieldType")
    private String mFieldType;
    @SerializedName("FieldValue")
    private String mFieldValue;

    public String getFieldLabel() {
        return mFieldLabel;
    }

    public void setFieldLabel(String FieldLabel) {
        mFieldLabel = FieldLabel;
    }

    public String getFieldNumber() {
        return mFieldNumber;
    }

    public void setFieldNumber(String FieldNumber) {
        mFieldNumber = FieldNumber;
    }

    public String getFieldType() {
        return mFieldType;
    }

    public void setFieldType(String FieldType) {
        mFieldType = FieldType;
    }

    public String getFieldValue() {
        return mFieldValue;
    }

    public void setFieldValue(String FieldValue) {
        mFieldValue = FieldValue;
    }

}
