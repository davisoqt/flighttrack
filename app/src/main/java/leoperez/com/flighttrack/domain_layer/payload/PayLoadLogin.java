
package leoperez.com.flighttrack.domain_layer.payload;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PayLoadLogin {

    @SerializedName("dataJSON")
    private DataJSON mDataJSON;
    @SerializedName("process_id")
    private String mProcessId="getFTLogin";

    public DataJSON getDataJSON() {
        return mDataJSON;
    }

    public String getProcessId() {
        return mProcessId;
    }

    public DataJSON getmDataJSON() {
        return mDataJSON;
    }

    public void setmDataJSON(DataJSON mDataJSON) {
        this.mDataJSON = mDataJSON;
    }

    public String getmProcessId() {
        return mProcessId;
    }

    public void setmProcessId(String mProcessId) {
        this.mProcessId = mProcessId;
    }
}
