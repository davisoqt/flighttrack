
package leoperez.com.flighttrack.domain_layer.response.save_crew;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ReturnDatum {

    @Expose
    private String Action;
    @Expose
    private String BatchNumber;
    @Expose
    private leoperez.com.flighttrack.domain_layer.response.save_crew.Result Result;
    @Expose
    private String SessionKey;
    @Expose
    private String Status;
    @Expose
    private String StatusMessage;

    public String getAction() {
        return Action;
    }

    public void setAction(String Action) {
        this.Action = Action;
    }

    public String getBatchNumber() {
        return BatchNumber;
    }

    public void setBatchNumber(String BatchNumber) {
        this.BatchNumber = BatchNumber;
    }

    public leoperez.com.flighttrack.domain_layer.response.save_crew.Result getResult() {
        return Result;
    }

    public void setResult(leoperez.com.flighttrack.domain_layer.response.save_crew.Result Result) {
        this.Result = Result;
    }

    public String getSessionKey() {
        return SessionKey;
    }

    public void setSessionKey(String SessionKey) {
        this.SessionKey = SessionKey;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

}
