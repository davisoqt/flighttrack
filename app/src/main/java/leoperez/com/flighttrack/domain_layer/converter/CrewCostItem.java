package leoperez.com.flighttrack.domain_layer.converter;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public class CrewCostItem {
    String crew_id;
    String date;
    String icao;
    String credit_card;

    public CrewCostItem(String crew_id, String date, String icao, String credit_card) {
        this.crew_id = crew_id;
        this.date = date;
        this.icao = icao;
        this.credit_card = credit_card;
    }

    public String getCrew_id() {
        return crew_id;
    }

    public void setCrew_id(String crew_id) {
        this.crew_id = crew_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getCredit_card() {
        return credit_card;
    }

    public void setCredit_card(String credit_card) {
        this.credit_card = credit_card;
    }
}
