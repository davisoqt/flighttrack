package leoperez.com.flighttrack.domain_layer.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Leo Perez Ortíz on 27/10/17.
 */

@Entity
public class Test {
    
    String test;

    @Generated(hash = 642932876)
    public Test(String test) {
        this.test = test;
    }

    @Generated(hash = 372557997)
    public Test() {
    }

    public String getTest() {
        return this.test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
