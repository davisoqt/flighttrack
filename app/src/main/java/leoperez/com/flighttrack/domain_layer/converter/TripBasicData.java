package leoperez.com.flighttrack.domain_layer.converter;

/**
 * Created by Leo Perez Ortíz on 8/11/17.
 */

public class TripBasicData {
    String trip_id;
    String date;
    String update;
    String air_craft;

    public TripBasicData(String trip_id, String date, String update, String air_craft) {
        this.trip_id = trip_id;
        this.date = date;
        this.update = update;
        this.air_craft = air_craft;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getAir_craft() {
        return air_craft;
    }

    public void setAir_craft(String air_craft) {
        this.air_craft = air_craft;
    }
}
