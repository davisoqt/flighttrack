
package leoperez.com.flighttrack.domain_layer.response.home.masterlist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CrewIDList {

    @SerializedName("CrewID")
    private ArrayList<String> mCrewID;

    public ArrayList<String> getCrewID() {
        return mCrewID;
    }

    public void setCrewID(ArrayList<String> CrewID) {
        mCrewID = CrewID;
    }

}
