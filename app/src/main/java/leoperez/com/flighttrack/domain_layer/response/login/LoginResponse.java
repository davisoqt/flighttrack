
package leoperez.com.flighttrack.domain_layer.response.login;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class LoginResponse {

    @SerializedName("error")
    private String mError;
    @SerializedName("failure")
    private String mFailure;
    @SerializedName("failureMessage")
    private String mFailureMessage;
    @SerializedName("inputProcessId")
    private String mInputProcessId;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("returnCode")
    private String mReturnCode;
    @SerializedName("returnData")
    private List<ReturnDatum> mReturnData;

    public String getError() {
        return mError;
    }

    public String getFailure() {
        return mFailure;
    }

    public String getFailureMessage() {
        return mFailureMessage;
    }

    public String getInputProcessId() {
        return mInputProcessId;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getReturnCode() {
        return mReturnCode;
    }

    public List<ReturnDatum> getReturnData() {
        return mReturnData;
    }

    public static class Builder {

        private String mError;
        private String mFailure;
        private String mFailureMessage;
        private String mInputProcessId;
        private String mMessage;
        private String mReturnCode;
        private List<ReturnDatum> mReturnData;

        public LoginResponse.Builder withError(String error) {
            mError = error;
            return this;
        }

        public LoginResponse.Builder withFailure(String failure) {
            mFailure = failure;
            return this;
        }

        public LoginResponse.Builder withFailureMessage(String failureMessage) {
            mFailureMessage = failureMessage;
            return this;
        }

        public LoginResponse.Builder withInputProcessId(String inputProcessId) {
            mInputProcessId = inputProcessId;
            return this;
        }

        public LoginResponse.Builder withMessage(String message) {
            mMessage = message;
            return this;
        }

        public LoginResponse.Builder withReturnCode(String returnCode) {
            mReturnCode = returnCode;
            return this;
        }

        public LoginResponse.Builder withReturnData(List<ReturnDatum> returnData) {
            mReturnData = returnData;
            return this;
        }

        public LoginResponse build() {
            LoginResponse LoginResponse = new LoginResponse();
            LoginResponse.mError = mError;
            LoginResponse.mFailure = mFailure;
            LoginResponse.mFailureMessage = mFailureMessage;
            LoginResponse.mInputProcessId = mInputProcessId;
            LoginResponse.mMessage = mMessage;
            LoginResponse.mReturnCode = mReturnCode;
            LoginResponse.mReturnData = mReturnData;
            return LoginResponse;
        }

    }

}
