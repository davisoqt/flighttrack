
package leoperez.com.flighttrack.domain_layer.response.trip;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ACCostFieldList implements Parcelable {

    @SerializedName("Amount")
    private String mAmount;
    @SerializedName("CCard")
    private String mCCard;
    @SerializedName("FieldLabel")
    private String mFieldLabel;
    @SerializedName("FieldNumber")
    private String mFieldNumber;
    @SerializedName("Notes")
    private String mNotes;

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String Amount) {
        mAmount = Amount;
    }

    public String getCCard() {
        return mCCard;
    }

    public void setCCard(String CCard) {
        mCCard = CCard;
    }

    public String getFieldLabel() {
        return mFieldLabel;
    }

    public void setFieldLabel(String FieldLabel) {
        mFieldLabel = FieldLabel;
    }

    public String getFieldNumber() {
        return mFieldNumber;
    }

    public void setFieldNumber(String FieldNumber) {
        mFieldNumber = FieldNumber;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String Notes) {
        mNotes = Notes;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAmount);
        dest.writeString(this.mCCard);
        dest.writeString(this.mFieldLabel);
        dest.writeString(this.mFieldNumber);
        dest.writeString(this.mNotes);
    }

    public ACCostFieldList() {
    }

    protected ACCostFieldList(Parcel in) {
        this.mAmount = in.readString();
        this.mCCard = in.readString();
        this.mFieldLabel = in.readString();
        this.mFieldNumber = in.readString();
        this.mNotes = in.readString();
    }

    public static final Parcelable.Creator<ACCostFieldList> CREATOR = new Parcelable.Creator<ACCostFieldList>() {
        @Override
        public ACCostFieldList createFromParcel(Parcel source) {
            return new ACCostFieldList(source);
        }

        @Override
        public ACCostFieldList[] newArray(int size) {
            return new ACCostFieldList[size];
        }
    };
}
