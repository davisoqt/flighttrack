
package leoperez.com.flighttrack.domain_layer.response.login;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ReturnDatum {

    @SerializedName("Action")
    private String mAction;
    @SerializedName("SessionKey")
    private String mSessionKey;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("StatusMessage")
    private String mStatusMessage;

    public String getAction() {
        return mAction;
    }

    public String getSessionKey() {
        return mSessionKey;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }

    public static class Builder {

        private String mAction;
        private String mSessionKey;
        private String mStatus;
        private String mStatusMessage;

        public ReturnDatum.Builder withAction(String Action) {
            mAction = Action;
            return this;
        }

        public ReturnDatum.Builder withSessionKey(String SessionKey) {
            mSessionKey = SessionKey;
            return this;
        }

        public ReturnDatum.Builder withStatus(String Status) {
            mStatus = Status;
            return this;
        }

        public ReturnDatum.Builder withStatusMessage(String StatusMessage) {
            mStatusMessage = StatusMessage;
            return this;
        }

        public ReturnDatum build() {
            ReturnDatum ReturnDatum = new ReturnDatum();
            ReturnDatum.mAction = mAction;
            ReturnDatum.mSessionKey = mSessionKey;
            ReturnDatum.mStatus = mStatus;
            ReturnDatum.mStatusMessage = mStatusMessage;
            return ReturnDatum;
        }

    }

}
