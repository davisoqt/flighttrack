
package leoperez.com.flighttrack.domain_layer.response.home.triplist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Trips {

    @SerializedName("TripDetails")
    private ArrayList<TripDetail> mTripDetails;

    public ArrayList<TripDetail> getTripDetails() {
        return mTripDetails;
    }

    public void setTripDetails(ArrayList<TripDetail> TripDetails) {
        mTripDetails = TripDetails;
    }

}
