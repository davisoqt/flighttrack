
package leoperez.com.flighttrack.domain_layer.payload.save_crew;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SaveCrew {

    @SerializedName("dataJSON")
    private leoperez.com.flighttrack.domain_layer.payload.save_crew.DataJSON DataJSON;
    @SerializedName("process_id")
    private String ProcessId="doFTPutCrew";

    public leoperez.com.flighttrack.domain_layer.payload.save_crew.DataJSON getDataJSON() {
        return DataJSON;
    }

    public void setDataJSON(leoperez.com.flighttrack.domain_layer.payload.save_crew.DataJSON dataJSON) {
        DataJSON = dataJSON;
    }

    public String getProcessId() {
        return ProcessId;
    }

    public void setProcessId(String processId) {
        ProcessId = processId;
    }

}
