
package leoperez.com.flighttrack.domain_layer.payload.save_crew;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Crew {

    @Expose
    private String ACM1Id;
    @Expose
    private String ACM2Id;
    @Expose
    private String ACM3Id;
    @Expose
    private String ACM4Id;
    @Expose
    private String PICFlying;
    @Expose
    private String PICId;
    @Expose
    private String SICFlying;
    @Expose
    private String SICId;

    public String getACM1Id() {
        return ACM1Id;
    }

    public void setACM1Id(String ACM1Id) {
        this.ACM1Id = ACM1Id;
    }

    public String getACM2Id() {
        return ACM2Id;
    }

    public void setACM2Id(String ACM2Id) {
        this.ACM2Id = ACM2Id;
    }

    public String getACM3Id() {
        return ACM3Id;
    }

    public void setACM3Id(String ACM3Id) {
        this.ACM3Id = ACM3Id;
    }

    public String getACM4Id() {
        return ACM4Id;
    }

    public void setACM4Id(String ACM4Id) {
        this.ACM4Id = ACM4Id;
    }

    public String getPICFlying() {
        return PICFlying;
    }

    public void setPICFlying(String PICFlying) {
        this.PICFlying = PICFlying;
    }

    public String getPICId() {
        return PICId;
    }

    public void setPICId(String PICId) {
        this.PICId = PICId;
    }

    public String getSICFlying() {
        return SICFlying;
    }

    public void setSICFlying(String SICFlying) {
        this.SICFlying = SICFlying;
    }

    public String getSICId() {
        return SICId;
    }

    public void setSICId(String SICId) {
        this.SICId = SICId;
    }

}
