package leoperez.com.flighttrack.domain_layer.converter;

import android.view.View;

/**
 * Created by Leo Perez Ortíz on 6/11/17.
 */

public class RowPosition {
    int position;
    View content;

    public RowPosition(int position, View content) {
        this.position = position;
        this.content = content;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public View getContent() {
        return content;
    }

    public void setContent(View content) {
        this.content = content;
    }
}