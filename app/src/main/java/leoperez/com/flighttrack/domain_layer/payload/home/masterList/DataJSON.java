
package leoperez.com.flighttrack.domain_layer.payload.home.masterList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataJSON {

    @SerializedName("Action")
    private String mAction="MasterList";
    @SerializedName("SessionKey")
    private String mSessionKey;

    public String getAction() {
        return mAction;
    }

    public void setAction(String Action) {
        mAction = Action;
    }

    public String getSessionKey() {
        return mSessionKey;
    }

    public void setSessionKey(String SessionKey) {
        mSessionKey = SessionKey;
    }

}
