
package leoperez.com.flighttrack.domain_layer.response.home.masterlist;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Result {

    @SerializedName("UpdateMasterList")
    private UpdateMasterList mUpdateMasterList;

    public UpdateMasterList getUpdateMasterList() {
        return mUpdateMasterList;
    }

    public void setUpdateMasterList(UpdateMasterList UpdateMasterList) {
        mUpdateMasterList = UpdateMasterList;
    }

}
