
package leoperez.com.flighttrack.domain_layer.response.trip;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CrewCostsList {

    @SerializedName("CCard")
    private String mCCard;
    @SerializedName("CostDate")
    private String mCostDate;
    @SerializedName("CrewCostFieldList")
    private List<leoperez.com.flighttrack.domain_layer.response.trip.CrewCostFieldList> mCrewCostFieldList;
    @SerializedName("CrewCostID")
    private String mCrewCostID;
    @SerializedName("CrewID")
    private String mCrewID;
    @SerializedName("ICAO")
    private String mICAO;
    @SerializedName("Notes")
    private String mNotes;

    public String getCCard() {
        return mCCard;
    }

    public void setCCard(String CCard) {
        mCCard = CCard;
    }

    public String getCostDate() {
        return mCostDate;
    }

    public void setCostDate(String CostDate) {
        mCostDate = CostDate;
    }

    public List<leoperez.com.flighttrack.domain_layer.response.trip.CrewCostFieldList> getCrewCostFieldList() {
        return mCrewCostFieldList;
    }

    public void setCrewCostFieldList(List<leoperez.com.flighttrack.domain_layer.response.trip.CrewCostFieldList> CrewCostFieldList) {
        mCrewCostFieldList = CrewCostFieldList;
    }

    public String getCrewCostID() {
        return mCrewCostID;
    }

    public void setCrewCostID(String CrewCostID) {
        mCrewCostID = CrewCostID;
    }

    public String getCrewID() {
        return mCrewID;
    }

    public void setCrewID(String CrewID) {
        mCrewID = CrewID;
    }

    public String getICAO() {
        return mICAO;
    }

    public void setICAO(String ICAO) {
        mICAO = ICAO;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String Notes) {
        mNotes = Notes;
    }

}
