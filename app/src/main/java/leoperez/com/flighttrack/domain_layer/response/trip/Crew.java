
package leoperez.com.flighttrack.domain_layer.response.trip;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Crew implements Parcelable {

    @SerializedName("ACM1Id")
    private String mACM1Id;
    @SerializedName("ACM2Id")
    private String mACM2Id;
    @SerializedName("ACM3Id")
    private String mACM3Id;
    @SerializedName("ACM4Id")
    private String mACM4Id;
    @SerializedName("CrewFieldList")
    private ArrayList<CrewFieldList> mCrewFieldList;
    @SerializedName("PICFlying")
    private String mPICFlying;
    @SerializedName("PICId")
    private String mPICId;
    @SerializedName("SICFlying")
    private String mSICFlying;
    @SerializedName("SICId")
    private String mSICId;

    public String getACM1Id() {
        return mACM1Id;
    }

    public void setACM1Id(String ACM1Id) {
        mACM1Id = ACM1Id;
    }

    public String getACM2Id() {
        return mACM2Id;
    }

    public void setACM2Id(String ACM2Id) {
        mACM2Id = ACM2Id;
    }

    public String getACM3Id() {
        return mACM3Id;
    }

    public void setACM3Id(String ACM3Id) {
        mACM3Id = ACM3Id;
    }

    public String getACM4Id() {
        return mACM4Id;
    }

    public void setACM4Id(String ACM4Id) {
        mACM4Id = ACM4Id;
    }

    public List<leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList> getCrewFieldList() {
        return mCrewFieldList;
    }

    public void setCrewFieldList(ArrayList<leoperez.com.flighttrack.domain_layer.response.trip.CrewFieldList> CrewFieldList) {
        mCrewFieldList = CrewFieldList;
    }

    public String getPICFlying() {
        return mPICFlying;
    }

    public void setPICFlying(String PICFlying) {
        mPICFlying = PICFlying;
    }

    public String getPICId() {
        return mPICId;
    }

    public void setPICId(String PICId) {
        mPICId = PICId;
    }

    public String getSICFlying() {
        return mSICFlying;
    }

    public void setSICFlying(String SICFlying) {
        mSICFlying = SICFlying;
    }

    public String getSICId() {
        return mSICId;
    }

    public void setSICId(String SICId) {
        mSICId = SICId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mACM1Id);
        dest.writeString(this.mACM2Id);
        dest.writeString(this.mACM3Id);
        dest.writeString(this.mACM4Id);
        dest.writeList(this.mCrewFieldList);
        dest.writeString(this.mPICFlying);
        dest.writeString(this.mPICId);
        dest.writeString(this.mSICFlying);
        dest.writeString(this.mSICId);
    }

    public Crew() {
    }

    protected Crew(Parcel in) {
        this.mACM1Id = in.readString();
        this.mACM2Id = in.readString();
        this.mACM3Id = in.readString();
        this.mACM4Id = in.readString();
        this.mCrewFieldList = new ArrayList<CrewFieldList>();
        in.readList(this.mCrewFieldList, CrewFieldList.class.getClassLoader());
        this.mPICFlying = in.readString();
        this.mPICId = in.readString();
        this.mSICFlying = in.readString();
        this.mSICId = in.readString();
    }

    public static final Parcelable.Creator<Crew> CREATOR = new Parcelable.Creator<Crew>() {
        @Override
        public Crew createFromParcel(Parcel source) {
            return new Crew(source);
        }

        @Override
        public Crew[] newArray(int size) {
            return new Crew[size];
        }
    };
}
