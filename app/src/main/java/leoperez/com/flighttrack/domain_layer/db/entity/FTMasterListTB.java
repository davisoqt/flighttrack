package leoperez.com.flighttrack.domain_layer.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Leo Perez Ortíz on 30/10/17.
 */

@Entity
public class FTMasterListTB {
    private String json;
    @Id
    private long id;
    @Generated(hash = 376861307)
    public FTMasterListTB(String json, long id) {
        this.json = json;
        this.id = id;
    }
    @Generated(hash = 1485143270)
    public FTMasterListTB() {
    }
    public String getJson() {
        return this.json;
    }
    public void setJson(String json) {
        this.json = json;
    }
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
}
