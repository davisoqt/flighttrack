
package leoperez.com.flighttrack.domain_layer.payload.save_crew;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataJSON {

    @Expose
    private String Action="PutCrew";
    @Expose
    private String BatchNumber="0";
    @Expose
    private leoperez.com.flighttrack.domain_layer.payload.save_crew.Request Request;
    @Expose
    private String SessionKey;

    public String getAction() {
        return Action;
    }

    public void setAction(String Action) {
        this.Action = Action;
    }

    public String getBatchNumber() {
        return BatchNumber;
    }

    public void setBatchNumber(String BatchNumber) {
        this.BatchNumber = BatchNumber;
    }

    public leoperez.com.flighttrack.domain_layer.payload.save_crew.Request getRequest() {
        return Request;
    }

    public void setRequest(leoperez.com.flighttrack.domain_layer.payload.save_crew.Request Request) {
        this.Request = Request;
    }

    public String getSessionKey() {
        return SessionKey;
    }

    public void setSessionKey(String SessionKey) {
        this.SessionKey = SessionKey;
    }

}
