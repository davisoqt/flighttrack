
package leoperez.com.flighttrack.domain_layer.response.trip;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Leg {

    @SerializedName("ACCostArrive")
    private leoperez.com.flighttrack.domain_layer.response.trip.ACCostArrive mACCostArrive;
    @SerializedName("ACCostDepart")
    private leoperez.com.flighttrack.domain_layer.response.trip.ACCostDepart mACCostDepart;
    @SerializedName("CheckedOutBy")
    private String mCheckedOutBy;
    @SerializedName("Crew")
    private leoperez.com.flighttrack.domain_layer.response.trip.Crew mCrew;
    @SerializedName("LegFieldListTrip")
    private List<leoperez.com.flighttrack.domain_layer.response.trip.LegFieldListTrip> mLegFieldListTrip;
    @SerializedName("LegID")
    private String mLegID;
    @SerializedName("LegNumber")
    private String mLegNumber;
    @SerializedName("PaxFieldNo")
    private String mPaxFieldNo;
    @SerializedName("PaxLabel1")
    private String mPaxLabel1;
    @SerializedName("PaxLabel2")
    private String mPaxLabel2;
    @SerializedName("PaxListTrip")
    private ArrayList<leoperez.com.flighttrack.domain_layer.response.trip.PaxListTrip> mPaxListTrip;

    public leoperez.com.flighttrack.domain_layer.response.trip.ACCostArrive getACCostArrive() {
        return mACCostArrive;
    }

    public void setACCostArrive(leoperez.com.flighttrack.domain_layer.response.trip.ACCostArrive ACCostArrive) {
        mACCostArrive = ACCostArrive;
    }

    public leoperez.com.flighttrack.domain_layer.response.trip.ACCostDepart getACCostDepart() {
        return mACCostDepart;
    }

    public void setACCostDepart(leoperez.com.flighttrack.domain_layer.response.trip.ACCostDepart ACCostDepart) {
        mACCostDepart = ACCostDepart;
    }

    public String getCheckedOutBy() {
        return mCheckedOutBy;
    }

    public void setCheckedOutBy(String CheckedOutBy) {
        mCheckedOutBy = CheckedOutBy;
    }

    public leoperez.com.flighttrack.domain_layer.response.trip.Crew getCrew() {
        return mCrew;
    }

    public void setCrew(leoperez.com.flighttrack.domain_layer.response.trip.Crew Crew) {
        mCrew = Crew;
    }

    public List<leoperez.com.flighttrack.domain_layer.response.trip.LegFieldListTrip> getLegFieldListTrip() {
        return mLegFieldListTrip;
    }

    public void setLegFieldListTrip(List<leoperez.com.flighttrack.domain_layer.response.trip.LegFieldListTrip> LegFieldListTrip) {
        mLegFieldListTrip = LegFieldListTrip;
    }

    public String getLegID() {
        return mLegID;
    }

    public void setLegID(String LegID) {
        mLegID = LegID;
    }

    public String getLegNumber() {
        return mLegNumber;
    }

    public void setLegNumber(String LegNumber) {
        mLegNumber = LegNumber;
    }

    public String getPaxFieldNo() {
        return mPaxFieldNo;
    }

    public void setPaxFieldNo(String PaxFieldNo) {
        mPaxFieldNo = PaxFieldNo;
    }

    public String getPaxLabel1() {
        return mPaxLabel1;
    }

    public void setPaxLabel1(String PaxLabel1) {
        mPaxLabel1 = PaxLabel1;
    }

    public String getPaxLabel2() {
        return mPaxLabel2;
    }

    public void setPaxLabel2(String PaxLabel2) {
        mPaxLabel2 = PaxLabel2;
    }

    public ArrayList<PaxListTrip> getPaxListTrip() {
        return mPaxListTrip;
    }

    public void setPaxListTrip(ArrayList<leoperez.com.flighttrack.domain_layer.response.trip.PaxListTrip> PaxListTrip) {
        mPaxListTrip = PaxListTrip;
    }

}
