
package leoperez.com.flighttrack.domain_layer.response.trip;

import android.os.Parcel;
import android.os.Parcelable;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CrewFieldList implements Parcelable {

    @SerializedName("FieldLabel")
    private String mFieldLabel;
    @SerializedName("FieldNumber")
    private String mFieldNumber;
    @SerializedName("FieldType")
    private String mFieldType;
    @SerializedName("FieldValue")
    private String mFieldValue;

    public String getFieldLabel() {
        return mFieldLabel;
    }

    public void setFieldLabel(String FieldLabel) {
        mFieldLabel = FieldLabel;
    }

    public String getFieldNumber() {
        return mFieldNumber;
    }

    public void setFieldNumber(String FieldNumber) {
        mFieldNumber = FieldNumber;
    }

    public String getFieldType() {
        return mFieldType;
    }

    public void setFieldType(String FieldType) {
        mFieldType = FieldType;
    }

    public String getFieldValue() {
        return mFieldValue;
    }

    public void setFieldValue(String FieldValue) {
        mFieldValue = FieldValue;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mFieldLabel);
        dest.writeString(this.mFieldNumber);
        dest.writeString(this.mFieldType);
        dest.writeString(this.mFieldValue);
    }

    public CrewFieldList() {
    }

    protected CrewFieldList(Parcel in) {
        this.mFieldLabel = in.readString();
        this.mFieldNumber = in.readString();
        this.mFieldType = in.readString();
        this.mFieldValue = in.readString();
    }

    public static final Parcelable.Creator<CrewFieldList> CREATOR = new Parcelable.Creator<CrewFieldList>() {
        @Override
        public CrewFieldList createFromParcel(Parcel source) {
            return new CrewFieldList(source);
        }

        @Override
        public CrewFieldList[] newArray(int size) {
            return new CrewFieldList[size];
        }
    };
}
