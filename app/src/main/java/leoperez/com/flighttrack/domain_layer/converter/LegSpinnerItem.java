package leoperez.com.flighttrack.domain_layer.converter;

/**
 * Created by Leo Perez Ortíz on 5/11/17.
 */

public class LegSpinnerItem {
    private String id;
    private String text;

    public LegSpinnerItem(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
