
package leoperez.com.flighttrack.domain_layer.response.home.triplist;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class TripDetail {

    @SerializedName("AircraftID")
    private String mAircraftID;
    @SerializedName("LastUpdated")
    private String mLastUpdated;
    @SerializedName("PostFlag")
    private String mPostFlag;
    @SerializedName("StartDate")
    private String mStartDate;
    @SerializedName("TripID")
    private String mTripID;

    public String getAircraftID() {
        return mAircraftID;
    }

    public void setAircraftID(String AircraftID) {
        mAircraftID = AircraftID;
    }

    public String getLastUpdated() {
        return mLastUpdated;
    }

    public void setLastUpdated(String LastUpdated) {
        mLastUpdated = LastUpdated;
    }

    public String getPostFlag() {
        return mPostFlag;
    }

    public void setPostFlag(String PostFlag) {
        mPostFlag = PostFlag;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String StartDate) {
        mStartDate = StartDate;
    }

    public String getTripID() {
        return mTripID;
    }

    public void setTripID(String TripID) {
        mTripID = TripID;
    }

}
