
package leoperez.com.flighttrack.domain_layer.payload.save_accost;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

import leoperez.com.flighttrack.domain_layer.response.trip.*;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Request {

    @Expose
    private ArrayList<leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList> ACCostFieldList;
    @Expose
    private String ADFlag;
    @Expose
    private String Amount;
    @Expose
    private String CCard;
    @Expose
    private String CostDate;
    @Expose
    private String FBO;
    @Expose
    private String LegNumber;
    @Expose
    private String Notes;
    @Expose
    private String Quantity;
    @Expose
    private String RetailCost;
    @Expose
    private String TripID;
    @Expose
    private String Units;

    public ArrayList<leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList> getACCostFieldList() {
        return ACCostFieldList;
    }

    public void setACCostFieldList(ArrayList<leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList> ACCostFieldList) {
        this.ACCostFieldList = ACCostFieldList;
    }

    public String getADFlag() {
        return ADFlag;
    }

    public void setADFlag(String ADFlag) {
        this.ADFlag = ADFlag;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getCCard() {
        return CCard;
    }

    public void setCCard(String CCard) {
        this.CCard = CCard;
    }

    public String getCostDate() {
        return CostDate;
    }

    public void setCostDate(String CostDate) {
        this.CostDate = CostDate;
    }

    public String getFBO() {
        return FBO;
    }

    public void setFBO(String FBO) {
        this.FBO = FBO;
    }

    public String getLegNumber() {
        return LegNumber;
    }

    public void setLegNumber(String LegNumber) {
        this.LegNumber = LegNumber;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getRetailCost() {
        return RetailCost;
    }

    public void setRetailCost(String RetailCost) {
        this.RetailCost = RetailCost;
    }

    public String getTripID() {
        return TripID;
    }

    public void setTripID(String TripID) {
        this.TripID = TripID;
    }

    public String getUnits() {
        return Units;
    }

    public void setUnits(String Units) {
        this.Units = Units;
    }

}
