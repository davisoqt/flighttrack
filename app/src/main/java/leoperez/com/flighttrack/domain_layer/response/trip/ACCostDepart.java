
package leoperez.com.flighttrack.domain_layer.response.trip;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ACCostDepart implements Parcelable {

    @SerializedName("ACCostFieldList")
    private List<leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList> mACCostFieldList;
    @SerializedName("Amount")
    private String mAmount;
    @SerializedName("CCard")
    private String mCCard;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("FBO")
    private String mFBO;
    @SerializedName("Leg")
    private String mLeg;
    @SerializedName("Notes")
    private String mNotes;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("RetailCost")
    private String mRetailCost;
    @SerializedName("Units")
    private String mUnits;

    public List<leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList> getACCostFieldList() {
        return mACCostFieldList;
    }

    public void setACCostFieldList(List<leoperez.com.flighttrack.domain_layer.response.trip.ACCostFieldList> ACCostFieldList) {
        mACCostFieldList = ACCostFieldList;
    }

    public String getAmount() {
        return mAmount;
    }

    public void setAmount(String Amount) {
        mAmount = Amount;
    }

    public String getCCard() {
        return mCCard;
    }

    public void setCCard(String CCard) {
        mCCard = CCard;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String Date) {
        mDate = Date;
    }

    public String getFBO() {
        return mFBO;
    }

    public void setFBO(String FBO) {
        mFBO = FBO;
    }

    public String getLeg() {
        return mLeg;
    }

    public void setLeg(String Leg) {
        mLeg = Leg;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String Notes) {
        mNotes = Notes;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String Quantity) {
        mQuantity = Quantity;
    }

    public String getRetailCost() {
        return mRetailCost;
    }

    public void setRetailCost(String RetailCost) {
        mRetailCost = RetailCost;
    }

    public String getUnits() {
        return mUnits;
    }

    public void setUnits(String Units) {
        mUnits = Units;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.mACCostFieldList);
        dest.writeString(this.mAmount);
        dest.writeString(this.mCCard);
        dest.writeString(this.mDate);
        dest.writeString(this.mFBO);
        dest.writeString(this.mLeg);
        dest.writeString(this.mNotes);
        dest.writeString(this.mQuantity);
        dest.writeString(this.mRetailCost);
        dest.writeString(this.mUnits);
    }

    public ACCostDepart() {
    }

    protected ACCostDepart(Parcel in) {
        this.mACCostFieldList = new ArrayList<ACCostFieldList>();
        in.readList(this.mACCostFieldList, ACCostFieldList.class.getClassLoader());
        this.mAmount = in.readString();
        this.mCCard = in.readString();
        this.mDate = in.readString();
        this.mFBO = in.readString();
        this.mLeg = in.readString();
        this.mNotes = in.readString();
        this.mQuantity = in.readString();
        this.mRetailCost = in.readString();
        this.mUnits = in.readString();
    }

    public static final Parcelable.Creator<ACCostDepart> CREATOR = new Parcelable.Creator<ACCostDepart>() {
        @Override
        public ACCostDepart createFromParcel(Parcel source) {
            return new ACCostDepart(source);
        }

        @Override
        public ACCostDepart[] newArray(int size) {
            return new ACCostDepart[size];
        }
    };
}
