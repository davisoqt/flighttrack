
package leoperez.com.flighttrack.domain_layer.response.home.masterlist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CCList {

    @SerializedName("CCName")
    private ArrayList<String> mCCName;

    public ArrayList<String> getCCName() {
        return mCCName;
    }

    public void setCCName(ArrayList<String> CCName) {
        mCCName = CCName;
    }

}
