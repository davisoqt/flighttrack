package leoperez.com.flighttrack.domain_layer.converter;

import java.util.ArrayList;

import leoperez.com.flighttrack.domain_layer.response.trip.CrewCostsList;
import leoperez.com.flighttrack.domain_layer.response.trip.Leg;
import leoperez.com.flighttrack.domain_layer.response.trip.Result;

/**
 * Created by Leo Perez Ortíz on 2/11/17.
 */

public class ConverterModel {
    public static LegItem converToLegItem(Leg leg){
        return new LegItem(leg.getLegNumber(),leg.getLegFieldListTrip().get(0).getFieldValue(),leg.getLegFieldListTrip().get(1).getFieldValue());
    }

    public static LegSpinnerItem getLegsSpinner(Leg result){
        return new LegSpinnerItem(result.getLegID(),result.getLegNumber());
    }

    public static CrewCostItem convertToCrewCostItem(CrewCostsList item){
        return new CrewCostItem(item.getCrewID(),item.getCostDate(),item.getICAO(),item.getCCard());
    }
}
