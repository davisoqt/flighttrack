
package leoperez.com.flighttrack.domain_layer.response.home.masterlist;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class UpdateMasterList {

    @SerializedName("CCList")
    private CCList mCCList;
    @SerializedName("CrewIDList")
    private CrewIDList mCrewIDList;
    @SerializedName("DateFormat")
    private String mDateFormat;
    @SerializedName("PaxIDList")
    private List<PaxIDList> mPaxIDList;
    @SerializedName("TripPDFList")
    private List<Object> mTripPDFList;

    public CCList getCCList() {
        return mCCList;
    }

    public void setCCList(CCList CCList) {
        mCCList = CCList;
    }

    public CrewIDList getCrewIDList() {
        return mCrewIDList;
    }

    public void setCrewIDList(CrewIDList CrewIDList) {
        mCrewIDList = CrewIDList;
    }

    public String getDateFormat() {
        return mDateFormat;
    }

    public void setDateFormat(String DateFormat) {
        mDateFormat = DateFormat;
    }

    public List<PaxIDList> getPaxIDList() {
        return mPaxIDList;
    }

    public void setPaxIDList(List<PaxIDList> PaxIDList) {
        mPaxIDList = PaxIDList;
    }

    public List<Object> getTripPDFList() {
        return mTripPDFList;
    }

    public void setTripPDFList(List<Object> TripPDFList) {
        mTripPDFList = TripPDFList;
    }

}
