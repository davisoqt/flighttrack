
package leoperez.com.flighttrack.domain_layer.response.home.triplist;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ReturnDatum {

    @SerializedName("Action")
    private String mAction;
    @SerializedName("Result")
    private leoperez.com.flighttrack.domain_layer.response.home.triplist.Result mResult;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("StatusMessage")
    private String mStatusMessage;

    public String getAction() {
        return mAction;
    }

    public void setAction(String Action) {
        mAction = Action;
    }

    public leoperez.com.flighttrack.domain_layer.response.home.triplist.Result getResult() {
        return mResult;
    }

    public void setResult(leoperez.com.flighttrack.domain_layer.response.home.triplist.Result Result) {
        mResult = Result;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String Status) {
        mStatus = Status;
    }

    public String getStatusMessage() {
        return mStatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        mStatusMessage = StatusMessage;
    }

}
