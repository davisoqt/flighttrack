
package leoperez.com.flighttrack.domain_layer.payload.trip;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PayLoadTripDetail {

    @SerializedName("dataJSON")
    private DataJSON mDataJSON;
    @SerializedName("process_id")
    private String mProcessId="getFTTrip";

    public DataJSON getDataJSON() {
        return mDataJSON;
    }

    public void setDataJSON(DataJSON dataJSON) {
        mDataJSON = dataJSON;
    }

    public String getProcessId() {
        return mProcessId;
    }

    public void setProcessId(String processId) {
        mProcessId = processId;
    }

}
