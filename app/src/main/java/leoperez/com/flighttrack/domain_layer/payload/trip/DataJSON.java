
package leoperez.com.flighttrack.domain_layer.payload.trip;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataJSON {

    @SerializedName("Action")
    private String mAction="GetTrip";
    @SerializedName("Request")
    private leoperez.com.flighttrack.domain_layer.payload.trip.Request mRequest;
    @SerializedName("SessionKey")
    private String mSessionKey;

    public String getAction() {
        return mAction;
    }

    public void setAction(String Action) {
        mAction = Action;
    }

    public leoperez.com.flighttrack.domain_layer.payload.trip.Request getRequest() {
        return mRequest;
    }

    public void setRequest(leoperez.com.flighttrack.domain_layer.payload.trip.Request Request) {
        mRequest = Request;
    }

    public String getSessionKey() {
        return mSessionKey;
    }

    public void setSessionKey(String SessionKey) {
        mSessionKey = SessionKey;
    }

}
