package leoperez.com.flighttrack.domain_layer.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by Leo Perez Ortíz on 31/10/17.
 */

@Entity
public class FTTripListTB {
    private String json;
    @Id
    private long id;
    @Generated(hash = 1482288603)
    public FTTripListTB(String json, long id) {
        this.json = json;
        this.id = id;
    }
    @Generated(hash = 153827567)
    public FTTripListTB() {
    }
    public String getJson() {
        return this.json;
    }
    public void setJson(String json) {
        this.json = json;
    }
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
}
